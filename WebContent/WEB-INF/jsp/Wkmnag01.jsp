<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<title>わくわく調整丸|わくわく調整表作成</title>
<link rel="stylesheet" href="/waku2/contents/css/adjustment_common.css">
<link rel="stylesheet" href="/waku2/contents/css/adjustment_sp.css" media="screen and (max-width: 767px)">
<script type="text/javascript" src="/waku2/contents/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/i18n/jquery-ui-i18n.min.js"></script>
<script type="text/javascript" src="/waku2/contents/js/adjustment_common.js"></script>
</head>

<body>
<div class="header-wrap">
<h1 class="tt"><img src="/waku2/contents/img/logo.png" alt="ロゴマーク"></h1>
</div>

<div class="contener-wrap">
<ul class="tab">
	<li class="select page-titl">わくわく調整表</li>
	<li class="page-titl tab-btn">参加メンバーを募る</li>
</ul>
<ul class="content">
<form method="post" accept-charset="UTF-8" novalidate="novalidate">

<%-- beanの取得 --%>
<c:set var="bean" value="${__bean__}"/>
<%-- errorの取得 --%>
<c:set var="errorList" value="${bean.getError()}"/>

<%-- Hidden --%>
<input type="hidden" name="cryptedEventId" value="${bean.getCryptedEventId()}">

<li>
	<div class="content-wrap clearfix">

	<%-- エラーメッセージ --%>
	<c:if test="${errorList != null && errorList.size() > 0}">
	<div class="error">
		<p class="mess">${errorList.size()}件のエラーがありました。以下をご確認の上、入力してください。</p>
		<c:forEach items="${errorList}" var="errorLine" varStatus="loop">
		<p>STEP${loop.count}）${errorLine}</p>
		</c:forEach>
	</div>
	</c:if>

	<div class="event-box-name">
	<h2 class="st-titl"><span>STEP1</span>イベント名</h2>
	<p class="note">※お疲れ様でした飲み会、打ち合わせ など<br>あるいは、みんなで調整したい事項を命名します</p>
	<input class="event-name" maxlength="100" type="text" name="eventName" value="${bean.getEventName()}">
	<p class="s-titl">メモ<small>（任意）</small></p>
	<p class="note">※調整したい内容についての説明メモを入力</p>
	<textarea rows="5" class="event-detail-textarea" name="eventMemo">${bean.getEventMemo()}</textarea>
	</div>
	<div class="event-box-candidate">
	<div class="box-candidate">
	<div class="item-candidate">
	<h2 class="st-titl"><span>STEP2</span>候補</h2>
	<p class="day">候補となる日程、日時、候補となる場所、もの など<br>
候補の区切りは改行で判断されます。</p>
	<textarea placeholder="例）
8/7(月) 20:00~
8/8(火) 20:00~
8/9(水) 21:00~" id="CANDIDATE" name="eventDateText">${bean.getEventDateText()}</textarea>
	</div>
	<div class="item-calendar">
	<p>↓日付をクリックすると日程に日時が入ります</p>
		<div id="datepicker" class="calendar">
		</div>
	</div>
	</div>
	<div class="event-box-deadline">
	<h2 class="st-titl"><span>STEP3</span>締切日<input type="date" name="applDeadline" value="${bean.getApplDeadline()}"></h2>
	</div>
	</div>
<div class="member-manager">
<h3 class="st-titl"><span>STEP4</span>管理者メールアドレスを指定して調整表を作成すると後でイベント内容の修正や参加者の情報をダウンロードすることが出来ます。</h3>
<h3 class="s-titl">管理者メールアドレスの登録</h3>
<p>イベントを管理する人のメールアドレスを入力してください。複数の場合はカンマで区切ります。</p>
<textarea rows="3" class="mail-textarea" name="eventMngrMailAddr" >${bean.getEventMngrMailAddr()}</textarea>

<h3 class="s-titl">イベントパスワード</h3>
<p>このイベントの管理者用のパスワードを入力してください<span>（英数10文字）</span></p>
<p><input type="password" name="eventPassword"></p>
</div>
	<div class="btn-box">
	<button class="btn" name="createWaku2Button" value="createWaku2Button">わくわく調整表の作成</button>
	</div>
	</div>
</li>
</form>
</ul>
</div>

<div class="footer-wrap">
<p>© 2017 HumanSystem Co., Ltd.</p>
</div>
</body>
</html>
