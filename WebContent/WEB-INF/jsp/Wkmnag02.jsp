<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<title>わくわく調整丸|わくわく調整表作成</title>
<link rel="stylesheet" href="/waku2/contents/css/adjustment_common.css">
<link rel="stylesheet" href="/waku2/contents/css/adjustment_sp.css" media="screen and (max-width: 767px)">
<script type="text/javascript" src="/waku2/contents/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/i18n/jquery-ui-i18n.min.js"></script>
<script type="text/javascript" src="/waku2/contents/js/adjustment_common.js"></script>
</head>

<body>
<div class="header-wrap">
<h1 class="tt"><img src="/waku2/contents/img/logo.png" alt="ロゴマーク"></h1>
</div>

<div class="contener-wrap">
<ul class="tab">
	<li class="page-titl">わくわく調整表</li>
	<li class="tab-btn page-titl select">参加メンバーを募る</li>
</ul>
<ul class="content">
<form method="post" accept-charset="UTF-8" novalidate="novalidate">

<%-- beanの取得 --%>
<c:set var="bean" value="${__bean__}"/>
<%-- errorの取得 --%>
<c:set var="errorList" value="${bean.getError()}"/>

<%-- Hidden --%>
<input type="hidden" name="cryptedEventId" value="${bean.getCryptedEventId()}">
<input type="hidden" name="eventKey" value="${bean.getEventKey()}">
<input type="hidden" name="eventMngrMailAddr" value="${bean.getEventMngrMailAddr()}">
<input type="hidden" name="contactChangeFlag" value="${bean.getContactChangeFlag()}">
<input type="hidden" name="applDeadline" value="${bean.getApplDeadline()}">
<input type="hidden" name="answeredMailaddress" value="${bean.getAnsweredMailaddress()}">


<li class="member-wrap">
	<div class="content-wrap clearfix">

	<input type="hidden" name="hideFlg" value="${checkFlg.val()}">

	<%-- エラーメッセージ --%>
	<c:if test="${errorList != null && errorList.size() > 0}">
	<div class="error">
		<p class="mess">${errorList.size()}件のエラーがありました。以下をご確認の上、入力してください。</p>
		<c:forEach items="${errorList}" var="errorLine" varStatus="loop">
		<p>STEP${loop.count}）${errorLine}</p>
		</c:forEach>
	</div>
	</c:if>

	<c:if test="${bean.getMemberContact() eq '1'}" >
	<div class="content-wrap hide" id="inverse">
		<h3 class="st-titl"><span>STEP5</span>わくわく調整表のURLをコピー</h3>
		<p>わくわく調整表ができました。以下のURLをコピーしてみんなに回答してもらいましょう。<br>参加者を限定してメールを送る場合は「参加メンバーへの連絡を行う」の入力を行います。</p>
		<div class="url-copy">
			<input type="text" name = "chouseiUrl" class = "url" value = "${bean.getChouseiUrl()}" readonly="readonly"><button name="copyUrl" value="copyUrl" class="copy-btn">コピー</button>
		</div>
	</div>
	</c:if>
	<c:if test="${bean.getMemberContact() eq '0'}" >
	<div class="content-wrap show" id="inverse">
	<h3 class="st-titl"><span>STEP5</span>わくわく調整表のURLをコピー</h3>
	<p>わくわく調整表ができました。以下のURLをコピーしてみんなに回答してもらいましょう。<br>参加者を限定してメールを送る場合は「参加メンバーへの連絡を行う」の入力を行います。</p>
	<div class="url-copy">
			<input type="text" name = "chouseiUrl" class = "url" value = "${bean.getChouseiUrl()}" readonly="readonly"><button name="copyUrl" value="copyUrl" class="copy-btn">コピー</button>
		</div>
	</div>
	</c:if>
	<div class="in-area">
	<h4 class="member-check">
	<c:if test="${bean.getContactChangeFlag() eq 'ON'}" >
		<label class="check">
			<c:if test="${bean.getMemberContact() eq '1'}">
				<input name= "memberContact" type="checkbox" class="checkbox participant" value="1" checked="checked"><span class="checkbox-icon"></span>参加メンバーへの連絡を行う
			</c:if>
			<c:if test="${bean.getMemberContact() eq '0'}">
				<input name= "memberContact" type="checkbox" class="checkbox participant" value="1"><span class="checkbox-icon"></span>参加メンバーへの連絡を行う
			</c:if>
		</label>
	</c:if>
	<c:if test="${bean.getContactChangeFlag() eq 'OFF'}" >
		<label class="check" onclick="return false;">
			<c:if test="${bean.getMemberContact() eq '1'}" >
				<input name= "memberContact" type="checkbox" class="checkbox participant" value="1" checked="checked"><span class="checkbox-icon" onclick="return false;"></span>参加メンバーへの連絡を行う
		</c:if>
			<c:if test="${bean.getMemberContact() eq '0'}" >
				<input name= "memberContact" type="checkbox" class="checkbox participant" value="1"><span class="checkbox-icon" onclick="return false;" onclick="return false;"></span>参加メンバーへの連絡を行う
		</c:if>
		</label>
	</c:if>
	</h4>
	<p>調整入力をお願いするメンバーへメールを送ります。お願いするメールテキストと送信先のメールアドレスを入力してください。<br>
	末尾に締切日とわくわく調整表ページのURLを自動的にセットして、お願いのメールを送信します。</p>
	<c:if test="${bean.getMemberContact() eq '1'}" >
		<div class="participant-wrap show">
		<h5 class="s-titl">メールテキスト<span>（1000文字以内）</span></h5>
		<textarea rows="3" class="mail-textarea" name="mailText">${bean.getMailText()}</textarea>
		<h5 class="s-titl">参加者メールアドレス</h5>
		<p>参加者のメールアドレスをカンマ区切りで入力します</p>
		<textarea rows="3" class="mail-textarea" name="participantMailaddress">${bean.getParticipantMailaddress()}</textarea>
		<h5 class="s-titl">回答済みメールアドレス</h5>
		<p>以下の参加者が回答しています</p>
		${bean.getAnsweredMailaddress()}
		</div>
	</c:if>
	<c:if test="${bean.getMemberContact() eq '0'}" >
<div class="participant-wrap hide">
	<h5 class="s-titl">メールテキスト<span>（1000文字以内）</span></h5>
	<textarea rows="3" class="mail-textarea" name="mailText">${bean.getMailText()}</textarea>
	<h5 class="s-titl">参加者メールアドレス</h5>
	<p>参加者のメールアドレスをカンマ区切りで入力します</p>
	<textarea rows="3" class="mail-textarea" name="participantMailaddress">${bean.getParticipantMailaddress()}</textarea>
	<h5 class="s-titl">回答済みメールアドレス</h5>
	<p>以下の参加者が回答しています</p>
	${bean.getAnsweredMailaddress()}
	</div>
	</c:if>
</div>

<div class="mail-check-box">
<label class="check">
<input type="checkbox" name="allMembersSendMail" value="1" class="checkbox" id="CHECK"><span class="checkbox-icon"></span>全員にメールを送る
</label>
<div class="btn-box">
<button name="registButton" value="registButton" class="btn"><span id="REG">登録</span></button>
</div>
</div>
</div>
</div>
</li>
</form>
</ul>
</div>

<div class="footer-wrap">
<p>© 2017 HumanSystem Co., Ltd.</p>
</div>
</body>
</html>
