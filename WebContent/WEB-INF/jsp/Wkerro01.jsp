<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<title>わくわく調整丸|ログイン</title>
<link rel="stylesheet" href="/waku2/contents/css/adjustment_common.css">
<link rel="stylesheet" href="/waku2/contents/css/adjustment_sp.css" media="screen and (max-width: 767px)">
<script src="/waku2/contents/js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="/waku2/contents/js/adjustment_common.js" type="text/javascript"></script>
</head>

<body>
<div class="header-wrap">
<h1 class="tt"><img src="/waku2/contents/img/logo.png" alt="ロゴマーク"></h1>
</div>

<div class="contener-wrap mess-wrap">
<div class="mess-area mess-err-area "><!-- エラー：mess-err-area --><!-- 完了：mess-fin-area -->
<div class="mess-box">
	<p class="main">システムエラーが発生しました。</p>
	<p class="sup">大変お手数ですが、サイト管理者までご連絡ください。</p>
</div>
</div>

<div class="btn-box">
<button class="btn m-btn" onclick="history.back()">前のページへ戻る</button>
</div>
</div>

<div class="footer-wrap">
<p>© 2017 HumanSystem Co., Ltd.</p>
</div>
</body>
</html>
