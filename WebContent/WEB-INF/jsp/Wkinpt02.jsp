<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<title>わくわく調整丸|○△×入力</title>
<link rel="stylesheet" href="/waku2/contents/css/adjustment_common.css">
<link rel="stylesheet" href="/waku2/contents/css/adjustment_sp.css" media="screen and (max-width: 767px)">
<script src="/waku2/contents/js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="/waku2/contents/js/adjustment_common.js" type="text/javascript"></script>
<script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/i18n/jquery-ui-i18n.min.js"></script>
<script src="js/adjustment_common.js" type="text/javascript"></script>
</head>

<body>
<div class="header-wrap">
<h1 class="tt"><img src="/waku2/contents/img/logo.png" alt="ロゴマーク"></h1>
</div>

<form method="post" accept-charset="UTF-8" novalidate="novalidate">


<%-- beanの取得 --%>

<c:set var="bean" value="${__bean__}"/>
<%-- errorの取得 --%>
<c:set var="errorList" value="${bean.getError()}"/>

<%-- hidden --%>
<input type="hidden" name="eventListSize" value="${bean.eventDateList.size()}">
<input type="hidden" name="cryptedEventId" value="${bean.cryptedEventId}">
<input type="hidden" name="applMailAddr" value="${bean.applMailAddr}">
<input type="hidden" name="cryptedEventAttendId" value="${bean.cryptedEventAttendId}">
<input type="hidden" name="eventKey" value="${bean.eventKey}">

<div class="contener-wrap input-wrap">
<h2 class="page-titl"><span class="maru"></span><span class="sankaku"></span><span class="batu"></span>を入力する</h2>
<div class="content-wrap">

<%-- エラーメッセージ --%>
	<c:if test="${errorList != null && errorList.size() > 0}">
	<div class="error">
		<p class="mess">${errorList.size()}件のエラーがありました。以下をご確認の上、入力してください。</p>
		<c:forEach items="${errorList}" var="errorLine" varStatus="loop">
		<p>STEP${loop.count}）${errorLine}</p>
		</c:forEach>
	</div>
	</c:if>

<h3 class="s-titl">表示名</h3>
<p>表示に使用する名前を入力してください。</p>
<input type="text" name="applName" value="${bean.applName}">
<div class="in-area">
<div class="in-box">
<h3 class="s-titl">候補</h3>
<table class="event-input-tab">
	<tbody>
		<c:forEach items= "${bean.eventDateList}" var="scheduleLine">
			<input type="hidden" name="eventDateText" value="${scheduleLine.eventDateText}">
			<c:if test="${scheduleLine.getApplType() eq '1'}">
				<tr>
					<input type="hidden" class="appl_type" name="applType" value="${scheduleLine.getApplType()}">
					<th>
						${scheduleLine.eventDateText}
					</th>
					<td class="1"><span class="maru selected"></span></td>
					<td class="2"><span class="sankaku"></span></td>
					<td class="3"><span class="batu"></span></td>

				</tr>
			</c:if>
			<c:if test="${scheduleLine.getApplType() eq '2'}">
				<tr>
					<input type="hidden" class="appl_type" name="applType" value="${scheduleLine.getApplType()}">
					<th>
						${scheduleLine.eventDateText}
					</th>
					<td class="1"><span class="maru"></span></td>
					<td class="2"><span class="sankaku selected"></span></td>
					<td class="3"><span class="batu"></span></td>

				</tr>
			</c:if>
			<c:if test="${scheduleLine.getApplType() eq '3'}">
				<tr>
					<input type="hidden" class="appl_type" name="applType" value="${scheduleLine.getApplType()}">
					<th>
						${scheduleLine.eventDateText}
					</th>
					<td class="1"><span class="maru"></span></td>
					<td class="2"><span class="sankaku"></span></td>
					<td class="3"><span class="batu selected"></span></td>

				</tr>
			</c:if>
			<c:if test="${scheduleLine.getApplType() eq ''}">
				<tr>
					<input type="hidden" class="appl_type" name="applType" value="3">
					<th>
						${scheduleLine.eventDateText}
					</th>
					<td class="1"><span class="maru"></span></td>
					<td class="2"><span class="sankaku"></span></td>
					<td class="3"><span class="batu selected"></span></td>

				</tr>
			</c:if>
			<input type="hidden" name="cryptedEventDateId" value="${scheduleLine.cryptedEventDateId}">
		</c:forEach>
	</tbody>
</table>
<div class="com-box">
<h3 class="s-titl">コメント</h3>
<textarea rows="5" class="com" name="applComment">${bean.applComment}</textarea>
</div>
</div>
</div>
<div class="btn-box">
	<button class="btn" name="registButton" value="registButton"><span class="maru"></span><span class="sankaku"></span><span class="batu"></span>を入力する</button>
</div>
</div>

</div>

</form>
<div class="footer-wrap">
<p>© 2017 HumanSystem Co., Ltd.</p>
</div>
</body>
</html>
