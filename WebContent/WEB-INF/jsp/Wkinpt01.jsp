<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>

<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<title>わくわく調整丸|わくわく調整表入力後</title>

<link rel="stylesheet" href="/waku2/contents/css/adjustment_common.css">
<link rel="stylesheet" href="/waku2/contents/css/adjustment_sp.css"
	media="screen and (max-width: 767px)">
<script src="/waku2/contents/js/jquery-1.11.2.min.js"
	type="text/javascript"></script>
<script src="/waku2/contents/js/adjustment_common.js"
	type="text/javascript"></script>
</head>
<body>
	<div class="header-wrap">
		<h1 class="tt">
			<img src="/waku2/contents/img/logo.png" alt="ロゴマーク">
		</h1>
	</div>

	<div class="contener-wrap event-wrap event-rear-wrap">

	<form method="post" accept-charset="UTF-8" novalidate="novalidate">

		<%-- beanの取得 --%>
		<c:set var="bean" value="${__bean__}" />
		<%-- errorの取得 --%>
		<c:set var="errorList" value="${bean.getError()}" />
		<%-- hidden --%>
		<input type="hidden" name="eventKey" value="${bean.getEventKey()}">
		<input type="hidden" name="cntctFlg" value="${bean.getCntctFlg()}">
		<input type="hidden" name="eventApplAddr" value="${bean.getEventApplAddr()}">

		<%-- エラーメッセージ --%>
		<c:if test="${errorList != null && errorList.size() > 0}">
		<div class="error">
			<p class="mess">${errorList.size()}件のエラーがありました。以下をご確認の上、入力してください。</p>
			<c:forEach items="${errorList}" var="errorLine" varStatus="loop">
			<p>STEP${loop.count}）${errorLine}</p>
			</c:forEach>
		</div>
		</c:if>

		<h2 class="page-titl">わくわく調整表</h2>
		<div class="content-wrap">

			<!-- イベント名 -->
			<h3 class="event-titl">${bean.getEventName()}</h3>

			<c:if test="${bean.getEventApplAddr() ne ''}" var="mailFlg" />

			<c:if test="${ mailFlg }">
				<p>回答者数：${bean.getEventDateList2().size()}人 / 全体数：${bean.getAllMemberCount()}人
				<fmt:formatNumber value="${bean.getEventDateList2().size() / bean.getAllMemberCount() * 100}" maxIntegerDigits="3" minIntegerDigits="1" maxFractionDigits="2" minFractionDigits="2" />
			%の回答率</p>
			</c:if>
			<c:if test="${ !mailFlg }">
				<p>回答者数：${bean.getEventDateList2().size()}人</p>
			</c:if>
			<h4 class="s-titl">メモ</h4>

			<p class="event-memo">${bean.getEventMemo()}</p>
			<div class="in-area">
				<h4 class="s-titl">候補</h4>
				<c:if test="${bean.getCntctFlg() eq '0'}">
					<p class="note">※各自の入力内容を変更するには名前のリンクをクリックしてください。</p>
				</c:if>
				<div class="scroll-area">
					<div class="event-rear-tab-scroll">

						<table class="event-tab event-rear-tab">
							<thead>
								<tr>
									<th>候補案</th>

									<th><span class="maru"></span></th>
									<th><span class="sankaku"></span></th>
									<th><span class="batu"></span></th>

									<!-- 実際はforeachで取得した人数分繰り返して名前を表示 -->
									<% boolean makeFlg = false; %>
									<c:forEach items="${bean.getEventDateList2()}" var="apname">
										<c:if test="${bean.getCntctFlg() eq '0'}">
											<c:if test="${mailFlg}">
											<c:if test="${bean.getEventApplAddr() eq apname.getEventApplAddr()}" var="flg" />
												<c:if test="${flg}">
												<th><a href="${apname.getRedirectURL()}">${apname.getApplName()}</a></th>
													<% makeFlg = true; %>
												</c:if>
												<c:if test="${!flg}">
													<th>${apname.getApplName()}</th>
												</c:if>
											</c:if>
											<c:if test="${!mailFlg}">
												<th><a href="${apname.getRedirectURL()}">${apname.getApplName()}</a></th>
											</c:if>
										</c:if>
										<c:if test="${bean.getCntctFlg() eq '1'}">
											<th>${apname.getApplName()}</th>
										</c:if>
									</c:forEach>
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${bean.getEventDateList()}" var="i">
									<c:set var="writeDate" value="${i.getEventDateID()}" />
									<tr>
										<th>${i.getEventDateText()}</th>
										<td>${i.getMaruCount()}人</td>
										<td>${i.getSankakuCount()}人</td>
										<td>${i.getBatuCount()}人</td>
										<c:forEach items="${bean.getEventDateList2()}" var="j">
											<c:set var="writeName" value="${j.getCryptedEventAttendid()}" />
											<% int cnt = 0; %>
										<c:forEach items="${bean.getEventDateList1()}" var="name">
												<c:if test="${name.getEventDateID() eq writeDate and (name.getCryptedEventAttendid() eq writeName)}">
													<% cnt = cnt + 1; %>
												<!-- 募集回答者名、繰り返す -->
												<td>
												<c:if test="${name.getApplType() eq '1'}" var="flg1">
													<span class="maru"></span>
												</c:if>
												<c:if test="${name.getApplType() eq '2'}" var="flg2">
													<span class="sankaku"></span>
												</c:if>
												<c:if test="${name.getApplType() eq '3'}"  var="flg3">
													<span class="batu"></span>
												</c:if>
												</td>
											</c:if>
										</c:forEach>
											<% if(cnt == 0) { %>
											<td></td>
											<% } %>
										</c:forEach>
									</tr>
								</c:forEach>
								<tr>
									<th class="com">コメント</th>
									<td colspan="3"></td>

									<c:forEach items="${bean.getEventDateList3()}" var="applmemo">
											<td>${applmemo.getApplComment()}</td>
										</c:forEach>
									<!-- 実際はDAO04で取得した結果のforeach -->

								</tr>
							</tbody>
						</table>

					</div>
				</div>

				<c:if test="${bean.getCntctFlg() eq '0'}">
					<% if (!makeFlg) { %>
						<p class="note note-rear">※予定の変更は表示名をクリックしてください</p>
						<div class="btn-box">
								<button class="btn" name="inputButton" value="inputButton">
									<span class="maru"></span><span class="sankaku"></span><span
										class="batu"></span>を入力する
								</button>
						</div>
					<% } %>
				</c:if>
			</div>
		</div>
	</form>
	</div>

	<div class="footer-wrap">
		<p>© 2017 HumanSystem Co., Ltd.</p>
	</div>
</body>
</html>