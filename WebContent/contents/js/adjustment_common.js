// JavaScript Document

 /*------------------------------------------
　　user agent
 --------------------------------------------*/
$(function () {
var ua = navigator.userAgent;
 var bw = window.navigator.userAgent.toLowerCase();

 if($('body').hasClass('pcview')){
 } else {
  /* iOSスマホ */
  if (ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 ){
   $('body').addClass('sp-vis ios');
  }
  /* Androidスマホ */
  else if (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0){
   $('body').addClass('sp-vis');

   if(isAndDefaultBrowser()==true && androidVersion() <= 4) {
    $('body').addClass('ad-df');
   } else {
    $('body').addClass('ad-ot');
   }
  }
  /* windows Phone */
  else if (ua.indexOf("windows") != -1 && ua.indexOf("phone") != -1){
   $('body').addClass('sp-vis winp');
  }
  /* タブレット */
  else if (ua.indexOf('iPad') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') == -1) || (ua.indexOf("windows") != -1 && ua.indexOf("touch") != -1) || ua.indexOf('A1_07') > 0 || ua.indexOf('SC-01C') > 0){
   $('body').addClass('tab');
   var metalist = document.getElementsByTagName('meta');
	for(var i = 0; i < metalist.length; i++) {
	    var name = metalist[i].getAttribute('name');
	    if(name && name.toLowerCase() === 'viewport') {
	        metalist[i].setAttribute('content','width=1000');
	        break;
	    }
	}
  }
  //IEの処理
  else if (bw.indexOf('msie') != -1 || bw.indexOf('trident') >= 0) {
   $('body').addClass('ie pc-vis');
  //IE6-7
   if (bw.indexOf("msie 7.") != -1 || bw.indexOf("msie 6.") != -1) {
     $('body').addClass('ie7');
   }
  //IE8
   else if (bw.indexOf("msie 8.") != -1) {
     $('body').addClass('ie8');
   }
  //IE9
   else if (bw.indexOf("msie 9.") != -1) {
     $('body').addClass('ie9');
   }
  //IE10
   else if (bw.indexOf("msie 10.") != -1) {
     $('body').addClass('ie10');
   }
  } else {
   $('body').addClass('pc-vis');
  }
  //webkit系
  if (bw.indexOf('chrome') != -1 || bw.indexOf('safari') != -1) {
   $('body').addClass('webkit');
  }
 }

 /* Android 標準ブラウザ */
 function isAndDefaultBrowser(){
  var ua=window.navigator.userAgent.toLowerCase();
  if(ua.indexOf('linux; u;')>0){
   return true;
  }else{
   return false;
  }
 }
 /* Android バージョン判定(不要なら削除) */
 function androidVersion() {
  var ua = navigator.userAgent;
  if( ua.indexOf("Android") > 0 ) {
   var version = parseFloat(ua.slice(ua.indexOf("Android")+8));
   return version;
  }
 }
});

/*------------------------------------------
　　ボタン文字変更
 --------------------------------------------*/
$(function() {
$('#CHECK').change(function(){
if ($(this).is(':checked')) {
$('#REG').text('登録して参加者にメールを送る');
} else {
$('#REG').text('登録');
}
});
});

/*------------------------------------------
　　カレンダーから日付候補選択
 --------------------------------------------*/
$(function() {
	//カレンダーの言語設定を日本語に変更
	$.datepicker.setDefaults($.datepicker.regional['ja']);
    $("#datepicker").datepicker({
			//出力表示
			dateFormat: 'm/d (D)',
			//カレンダー上で選択可能な最小の日(選択する日)
			minDate: new Date(),
			//カレンダー上部の年後ろに月を表示させる
			showMonthAfterYear: true,
			//過ぎた前の月には戻れないように矢印を表示させない
			hideIfNoPrevNext: true,

			onSelect: function(dateText) {
				//今のtextareaのデータ=nowcandidate
				var nowcandidate=$("#CANDIDATE").val();
				//もし、今のtextareaにデータ(nowcandidate)が空ならば("")、データを入れる
				if(nowcandidate==""){
					 $("#CANDIDATE").val(dateText+" 20:00~");
				//もし、入っているならば今入っているデータ(nowcandidate)を改行(\n)してから次のデータを追記
			}else{
					$("#CANDIDATE").val(nowcandidate+"\n"+dateText+" 20:00~");
			}
			}
    });
});

/*------------------------------------------
　　○△×ボタン押下時処理
 --------------------------------------------*/
$(function() {
  // 「○△×」ボタン押下時発火
  $('.event-input-tab tbody td span').on('click',function(){
    // 「選択済み」CSSクラス削除
    $(this).parents('tr').find('span').each(function(i, elem) {
      $(elem).removeClass('selected');
    });

    // 「選択済み」CSSクラス付与
    $(this).addClass('selected');

    // 選択ボタンによってHiddenに値格納
    var value = $(this).parents('td').attr('class');
    $(this).parents('tr').find('input[type="hidden"]').val(value);
  });
});

/*------------------------------------------
　　参加メンバーへの連絡を行うチェックボックス押下時処理
 --------------------------------------------*/
$(function() {
  // 参加メンバーへの連絡を行うチェックボックス押下時発火
  $('input[type="checkbox"].participant').on('click',function(){
    // チェックボックスオン/オフ判定
    if ( $(this).prop('checked') ) {
      $('div.participant-wrap').show();
      $('#inverse').hide();
    } else {
      $('div.participant-wrap').hide();
      $('#inverse').show();
    }
  });
});