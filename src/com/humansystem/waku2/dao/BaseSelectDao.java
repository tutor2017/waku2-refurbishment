package com.humansystem.waku2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.exception.Waku2RuntimeException;

/**
 * create-date 27/04/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
abstract class BaseSelectDao {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(BaseSelectDao.class);

	/**
	 * 実行するSQL
	 * @return
	 */
	abstract String getSQL();

	/**
	 * Select句にて取得する情報をMapへ格納する際のキー名のリスト
	 * @return
	 */
	abstract List<String> getColNames();

	/**
	 * Select処理
	 *
	 * @param con
	 * @param bindValues
	 * @return
	 */
	public List<Map<String, String>> select(Connection con, List<String> bindValues) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "select()");

		// デバッグ用
		StringBuffer buf = new StringBuffer();

		// データ格納
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();

		try (PreparedStatement statement = ((Connection) con).prepareStatement(getSQL())){

			if(bindValues != null){
				int idx = 1;
				for (String val : bindValues) {
					statement.setString(idx++, val);
				}
			}

			// デバッグログ
			log.debug("SQL [" + getSQL().replaceAll("\\?", "{}") + "]", bindValues.toArray());

			try(ResultSet rs = statement.executeQuery()){
				while (rs.next()) {
					buf.append("{");
					// 1件分のデータ
					Map<String, String> hdata = new HashMap<String, String>();
					// フィールドの数だけまわして、フィールド名とデータのマップを作成
					for (int i = 1; i <= getColNames().size(); i++) {
						// フィールド名
						String key = getColNames().get(i-1);

						// フィールド名に対するデータ
						String val = rs.getString(key);
						if (val == null) {
							val = "";
						}
						// データ格納(フィールド名, データ)
						hdata.put(key, val);
						buf.append(key + "=" + val + ",");
					}
					// 1件分のデータを格納
					result.add(hdata);
					buf.append("},");
				}
			}

		} catch (SQLException e) {
			throw new Waku2RuntimeException(ImmutableValues.get("MSGFTL0006"), e);
		}

		// デバッグログ
		log.debug("return [" + buf.toString() + "]");

		//終了ログ
		log.info(ImmutableValues.MSGLOG0006, "select()");

		return result;
	}

}
