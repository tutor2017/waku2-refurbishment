package com.humansystem.waku2.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKMNAG01_DF_03
 *
 * create-date 02/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
public class Wkmnag01Df03Dao extends BaseInsertDao {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag01Df03Dao.class);

	@Override
	String getSQL() {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "insert into T_EVENT "
				+ "( EVENT_KEY, EVENT_NAME, APPL_DEADLINE, EVENT_MEMO, EVENT_MNGR_MAIL_ADDR, EVENT_PASSWORD, CNTCT_FLG ) "
				+ "values (?, ?, ?, ?, ?, ?, ?)";
	}

}
