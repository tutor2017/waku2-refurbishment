package com.humansystem.waku2.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKMNAG01_DF_07
 *
 * create-date 02/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
public class Wkmnag01Df07Dao extends BaseUpdateDao {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag01Df07Dao.class);

	@Override
	String getSQL() {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "update T_EVENT_DATE set DELETE_FLG = '1' where EVENT_ID = ? ORDER BY SORT_NO";
	}
}
