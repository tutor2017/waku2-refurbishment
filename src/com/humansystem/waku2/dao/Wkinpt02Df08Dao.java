package com.humansystem.waku2.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKINPT02_DF_08
 *
 * create-date 16/06/2017.
 *
 * @author kasai_r
 */

public class Wkinpt02Df08Dao extends BaseSelectDao {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkinpt02Df08Dao.class);

	/** Field */
	public static final String APPL_NAME ="APPL_NAME";			// 申込者名
	public static final String APPL_COMMENT = "APPL_COMMENT";	// 申込者コメント

	@Override
	String getSQL() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "SELECT APPL_NAME, APPL_COMMENT "
				+ "FROM T_EVENT_ATTEND "
				+ "WHERE EVENT_ATTEND_ID = ?"
				+ "AND EVENT_ID = ?";
	}
	@Override
	List<String> getColNames() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getColNames()");

		List<String> ret = new ArrayList<>();
		ret.add(APPL_NAME);
		ret.add(APPL_COMMENT);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getColNames()");

		return ret;
	}
}
