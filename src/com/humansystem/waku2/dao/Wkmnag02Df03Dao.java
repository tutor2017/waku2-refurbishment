package com.humansystem.waku2.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKMNAG02_DF_03
 *
 * create-date 21/05/2017.
 *
 * @author Morita_K
 * check:Mizobe_n
 */

public class Wkmnag02Df03Dao extends BaseUpdateDao{

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag02Df03Dao.class);

	@Override
	String getSQL() {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "UPDATE T_EVENT SET CNTCT_FLG = ?, CNTCT_MAIL_BODY = ?, CNTCT_MAIL_ADDR = ? WHERE EVENT_ID = ?";
	}

}
