package com.humansystem.waku2.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKMNAG01_DF_06
 *
 * create-date 02/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
public class Wkmnag01Df06Dao extends BaseUpdateDao {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag01Df06Dao.class);

	@Override
	String getSQL() {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "update T_EVENT_DATE set SORT_NO = ?, DELETE_FLG = '0' where EVENT_ID = ? AND EVENT_DATE_TEXT = ?";
	}
}
