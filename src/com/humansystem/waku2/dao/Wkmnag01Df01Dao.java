package com.humansystem.waku2.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKMNAG01_DF_01
 *
 * create-date 02/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
public class Wkmnag01Df01Dao extends BaseSelectDao {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag01Df01Dao.class);

	public static final String EVENT_NAME = "EVENT_NAME";
	public static final String EVENT_MEMO = "EVENT_MEMO";
	public static final String APPL_DEADLINE = "APPL_DEADLINE";
	public static final String EVENT_MNGR_MAIL_ADDR = "EVENT_MNGR_MAIL_ADDR";
	public static final String EVENT_KEY = "EVENT_KEY";

	@Override
	List<String> getColNames(){

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getColNames()");

		List<String> ret = new ArrayList<>();
		ret.add(EVENT_NAME);
		ret.add(EVENT_MEMO);
		ret.add(APPL_DEADLINE);
		ret.add(EVENT_MNGR_MAIL_ADDR);
		ret.add(EVENT_KEY);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getColNames()");

		return ret;
	}

	@Override
	String getSQL() {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "select EVENT_NAME ,EVENT_MEMO ,APPL_DEADLINE ,EVENT_MNGR_MAIL_ADDR ,EVENT_KEY from T_EVENT where EVENT_ID = ?";
	}

}
