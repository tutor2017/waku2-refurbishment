package com.humansystem.waku2.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * Wkinpt02/○△✕入力画面のDaoクラス
 *
 * @author mizobe_n (revision:kasai_r)
 */
public class Wkinpt02Df01Dao extends BaseSelectDao{

	private static Logger log = LoggerFactory.getLogger(Wkinpt02Df01Dao.class);

	public static final String EVENT_ID = "EVENT_ID";

	@Override
	List<String> getColNames() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getColNames()");

		List<String> ret = new ArrayList<>();

		ret.add(EVENT_ID);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getColNames()");

		return ret;
	}


	@Override
	String getSQL() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "SELECT EVENT_ID "
				+ "FROM T_EVENT "
				+ "WHERE EVENT_KEY = ? "
				+ "AND APPL_DEADLINE > DATEADD('DAY', -1, CURRENT_DATE())";
	}



}