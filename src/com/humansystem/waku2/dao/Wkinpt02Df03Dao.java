package com.humansystem.waku2.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * Wkinpt02/○△✕入力画面のDaoクラス
 *
 * @author mizobe_n (revision:kasai_r)
 */
public class Wkinpt02Df03Dao extends BaseSelectDao{

	private static Logger log = LoggerFactory.getLogger(Wkinpt02Df03Dao.class);

	public static final String EVENT_DATE_ID = "EVENT_DATE_ID";
	public static final String EVENT_DATE_TEXT = "EVENT_DATE_TEXT";

	@Override
	List<String> getColNames() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getColNames()");

		List<String> ret = new ArrayList<>();

		ret.add(EVENT_DATE_ID);
		ret.add(EVENT_DATE_TEXT);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getColNames()");

		return ret;
	}


	@Override
	String getSQL() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "SELECT EVENT_DATE_ID, EVENT_DATE_TEXT FROM T_EVENT_DATE "
				+ "WHERE EVENT_ID = ? AND DELETE_FLG = 0 ORDER BY SORT_NO ASC";
	}



}