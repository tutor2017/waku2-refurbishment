package com.humansystem.waku2.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * create-date 28/04/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
abstract class BaseInsertDao extends BaseUpdateDao {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(BaseInsertDao.class);

	/** 最後に自動採番された値を取得するSQL */
	private static final String SELECT_SQL = "call SCOPE_IDENTITY()";

	/** 最後に自動採番された値を持つ列名 */
	private static final String IDENTITY_NAME = "SCOPE_IDENTITY()";

	/**
	 * 自動採番された値を取得する
	 * @param conn
	 * @return
	 */
	public String getIdentity(Connection conn){

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getIsdentity()");

		// 最後に自動採番された値を取得する。
		SelectIdentity selDao = new SelectIdentity();
		List<Map<String, String>> result = selDao.select(conn, new ArrayList<>());

		String identity = null;

		if (result != null && result.size() > 0) {
			Map<String, String> m = result.get(0);
			identity = m.get(IDENTITY_NAME);
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getIdentity()");

		return identity;
	}


	private class SelectIdentity extends BaseSelectDao {

		@Override
		String getSQL() {
			return SELECT_SQL;
		}

		@Override
		List<String> getColNames() {
			List<String> ret = new ArrayList<>();
			ret.add(IDENTITY_NAME);
			return ret;
		}

	}


}
