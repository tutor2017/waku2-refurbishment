package com.humansystem.waku2.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKINPT02_DF_07
 * @author 作成者 溝部 修正者 森川
 */
public class Wkinpt02Df07Dao extends BaseInsertDao {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkinpt02Df07Dao.class);

	@Override
	String getSQL() {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "INSERT INTO T_EVENT_ATTEND_DATE (EVENT_DATE_ID, EVENT_ATTEND_ID, APPL_TYPE) VALUES (?, ?, ?)";
	}
}
