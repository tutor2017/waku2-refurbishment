package com.humansystem.waku2.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKMNAG01_DF_02
 *
 * create-date 02/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
public class Wkmnag01Df02Dao extends BaseSelectDao {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag01Df02Dao.class);

	public static final String EVENT_DATE_TEXT = "EVENT_DATE_TEXT";

	@Override
	List<String> getColNames() {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getColNames()");

		List<String> ret = new ArrayList<>();
		ret.add(EVENT_DATE_TEXT);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getColNames()");

		return ret;
	}

	@Override
	String getSQL() {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "select EVENT_DATE_TEXT from T_EVENT_DATE where EVENT_ID = ? and DELETE_FLG = 0 order by SORT_NO asc;";
	}

}
