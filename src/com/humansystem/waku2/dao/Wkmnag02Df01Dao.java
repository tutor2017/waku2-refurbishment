package com.humansystem.waku2.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKMNAG02_DF_01
 *
 * create-date 22/05/2017.
 *
 *@author Morita_k
 *check:Mizobe_n
 */

public class Wkmnag02Df01Dao extends BaseSelectDao {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag02Df01Dao.class);

	public static final String EVENT_KEY = "EVENT_KEY";
	public static final String EVENT_MEMO = "EVENT_MEMO";
	public static final String EVENT_MNGR_MAIL_ADDR = "EVENT_MNGR_MAIL_ADDR";
	public static final String CNTCT_FLG = "CNTCT_FLG";
	public static final String CNTCT_MAIL_BODY = "CNTCT_MAIL_BODY";
	public static final String CNTCT_MAIL_ADDR = "CNTCT_MAIL_ADDR";
	public static final String APPL_DEADLINE = "APPL_DEADLINE";

	@Override
	List<String> getColNames(){

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getColNames()");

		List<String> ret = new ArrayList<>();
		ret.add(EVENT_KEY);
		ret.add(EVENT_MEMO);
		ret.add(EVENT_MNGR_MAIL_ADDR);
		ret.add(CNTCT_FLG);
		ret.add(CNTCT_MAIL_BODY);
		ret.add(CNTCT_MAIL_ADDR);
		ret.add(APPL_DEADLINE);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getColNames()");

		return ret;
	}

	@Override
	String getSQL() {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "select EVENT_KEY ,EVENT_MEMO ,EVENT_MNGR_MAIL_ADDR ,CNTCT_FLG ,CNTCT_MAIL_BODY ,CNTCT_MAIL_ADDR ,APPL_DEADLINE from T_EVENT where EVENT_ID = ?";
	}

}
