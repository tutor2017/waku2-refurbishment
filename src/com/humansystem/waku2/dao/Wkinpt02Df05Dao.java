package com.humansystem.waku2.dao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;
/**
 * WKINPT02_DF_05
 *
 * @author 作成者:ishii_y 修正者:morita_k
 */

public class Wkinpt02Df05Dao extends BaseInsertDao {

/** Logger */
private static Logger log = LoggerFactory.getLogger(Wkinpt02Df05Dao.class);
@Override
String getSQL() {
// 開始ログ
log.info(ImmutableValues.MSGLOG0005, "getSQL()");

// 終了ログ
log.info(ImmutableValues.MSGLOG0006, "getSQL()");

return "INSERT INTO T_EVENT_ATTEND "
		+ "( EVENT_ID ,APPL_NAME ,APPL_COMMENT ,APPL_MAIL_ADDR ) "
		+ "VALUES (?, ?, ?, ?)";

	}
}