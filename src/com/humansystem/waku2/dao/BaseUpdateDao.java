package com.humansystem.waku2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.exception.Waku2RuntimeException;

/**
 * create-date 27/04/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
abstract class BaseUpdateDao {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(BaseUpdateDao.class);

	/**
	 * 実行するSQL
	 * @return
	 */
	abstract String getSQL();

	/**
	 * Update処理
	 *
	 * @param conn
	 * @param bindValues
	 * @return 更新件数
	 */
	public int update(Connection conn, List<String> bindValues) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "update()");

		int retCnt = 0;

		try (PreparedStatement statement = conn.prepareStatement(getSQL())){

			if(bindValues !=null ){
				int idx = 1;
				for (String val : bindValues) {
					statement.setString(idx++, val);
				}
			}

			// デバッグログ
			log.debug("SQL [" + getSQL().replaceAll("\\?", "{}") + "]", bindValues.toArray());

			retCnt = statement.executeUpdate();

		} catch (SQLException e) {
			throw new Waku2RuntimeException(ImmutableValues.get("MSGFTL0006"), e);
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "update()");

		return retCnt;

	}

}
