package com.humansystem.waku2.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKINPT02_DF_06
 * @author 作成者 溝部 修正者 森川
 */


public class Wkinpt02Df06Dao extends BaseUpdateDao {
/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkinpt02Df06Dao.class);

	@Override

	String getSQL() {



	// 開始ログ
	log.info(ImmutableValues.MSGLOG0005, "getSQL()");


	// 終了ログ
	log.info(ImmutableValues.MSGLOG0006, "getSQL()");

	return "UPDATE T_EVENT_ATTEND_DATE SET APPL_TYPE = ? "
			+ "WHERE EVENT_DATE_ID = ? AND EVENT_ATTEND_ID = ?" ;
	}


}