package com.humansystem.waku2.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKINPT01_DF_02
 *
 * create-date 09/06/2017.
 *
 * @author 作成者:ishii_y 修正者:morita_k
 */
public class Wkinpt01Df02Dao extends BaseSelectDao{

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkinpt01Df02Dao.class);

	/** Field */
	public static final String EVENT_DATE_ID = "EVENT_DATE_ID"; 	 // イベント日程ID
	public static final String EVENT_DATE_TEXT = "EVENT_DATE_TEXT"; // 候補

	@Override
	List<String> getColNames() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getColNames()");

		List<String> ret = new ArrayList<>();
		ret.add(EVENT_DATE_ID);
		ret.add(EVENT_DATE_TEXT);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getColNames()");

		return ret;
	}

	@Override
	String getSQL() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return  "SELECT EVENT_DATE_ID, EVENT_DATE_TEXT "
				+ "FROM T_EVENT_DATE "
				+ "WHERE EVENT_ID = ? AND DELETE_FLG = 0 ORDER BY SORT_NO ASC";

	}
}