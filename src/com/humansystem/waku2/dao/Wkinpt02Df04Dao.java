package com.humansystem.waku2.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKINPT02_DF_04
 *
 * create-date 20/05/2017.
 *
 * @author 作成者:ishii_y 修正者:morita_k
 */
public class Wkinpt02Df04Dao extends BaseUpdateDao {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkinpt02Df04Dao.class);

	@Override
	String getSQL() {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "UPDATE T_EVENT_ATTEND " + "SET APPL_NAME = ?, APPL_COMMENT = ? " + "WHERE EVENT_ATTEND_ID = ?";
	}

}