package com.humansystem.waku2.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKINPT02_DF_02 ○△✕入力のdao02
 *
 * @author mizobe_n (revision:kasai_r)
 */
public class Wkinpt02Df02Dao extends BaseSelectDao {

	private static Logger log = LoggerFactory.getLogger(Wkinpt02Df02Dao.class);

	public static final String EVENT_DATE_ID = "EVENT_DATE_ID";
	public static final String EVENT_DATE_TEXT = "EVENT_DATE_TEXT";
	public static final String APPL_NAME ="APPL_NAME";			// 申込者名
	public static final String APPL_COMMENT = "APPL_COMMENT";	// 申込者コメント
	public static final String APPL_TYPE = "APPL_TYPE";

	@Override
	String getSQL() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "SELECT TED.EVENT_DATE_ID, TED.EVENT_DATE_TEXT, TEA.APPL_NAME, TEA.APPL_COMMENT, TEAD.APPL_TYPE "
				+ "FROM T_EVENT_DATE TED LEFT JOIN T_EVENT_ATTEND TEA "
				+ "ON TED.EVENT_ID = TEA.EVENT_ID "
				+ "AND TED.DELETE_FLG = 0 "
				+ "LEFT JOIN T_EVENT_ATTEND_DATE TEAD "
				+ "ON TEAD.EVENT_ATTEND_ID = TEA.EVENT_ATTEND_ID "
				+ "AND TED.EVENT_DATE_ID = TEAD.EVENT_DATE_ID "
				+ "WHERE TED.EVENT_ID = ? "
				+ "AND TEA.EVENT_ATTEND_ID = ? "
				+ "ORDER BY TED.SORT_NO ASC";
	}

	@Override
	List<String> getColNames() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getColNames()");

		List<String> ret = new ArrayList<>();

		ret.add(EVENT_DATE_ID);
		ret.add(EVENT_DATE_TEXT);
		ret.add(APPL_NAME);
		ret.add(APPL_COMMENT);
		ret.add(APPL_TYPE);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getColNames()");
		return ret;
	}

}
