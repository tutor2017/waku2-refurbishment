package com.humansystem.waku2.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKMNAG02_DF_02
 *
 * create-date 21/05/2017.
 *
 * @author 作成者 石井 修正者 森川
 */

public class Wkinpt01Df01Dao extends BaseSelectDao{
	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkinpt01Df01Dao.class);



	public static final String EVENT_NAME = "EVENT_NAME";
	public static final String EVENT_MEMO = "EVENT_MEMO";
	public static final String EVENT_ID = "EVENT_ID";
	public static final String APPL_DEADLINE = "APPL_DEADLINE";
	public static final String EVENT_MNGR_MAIL_ADDR = "EVENT_MNGR_MAIL_ADDR";
	public static final String CNTCT_FLG = "CNTCT_FLG";
	public static final String CNTCT_MAIL_ADDR = "CNTCT_MAIL_ADDR";

	@Override
	List<String> getColNames(){

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getColNames()");

		List<String> ret = new ArrayList<>();

		ret.add(EVENT_NAME);
		ret.add(EVENT_MEMO);
		ret.add(EVENT_ID);
		ret.add(APPL_DEADLINE);
		ret.add(EVENT_MNGR_MAIL_ADDR);
		ret.add(CNTCT_FLG);
		ret.add(CNTCT_MAIL_ADDR);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getColNames()");

		return ret;

	}

	@Override
	String getSQL() {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "SELECT EVENT_NAME, EVENT_MEMO, EVENT_ID, APPL_DEADLINE, EVENT_MNGR_MAIL_ADDR, CNTCT_FLG, CNTCT_MAIL_ADDR FROM T_EVENT WHERE EVENT_KEY = ?";
	}
}