package com.humansystem.waku2.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKLGIN01/管理者ログイン画面のDaoクラス
 *
 * create-date 18/05/2017.
 *
 * @author Morita_k
 * check:Mizobe_n
 */
public class Wklgin01Df01Dao extends BaseSelectDao{

	private static Logger log = LoggerFactory.getLogger(Wklgin01Df01Dao.class);

	public static final String EVENT_NAME = "EVENT_NAME";

	public static final String EVENT_ID = "EVENT_ID";

	@Override
	String getSQL() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "SELECT EVENT_NAME, EVENT_ID FROM T_EVENT WHERE CASE WHEN NOT ? = '' THEN EVENT_KEY = ? AND EVENT_PASSWORD = ? ELSE EVENT_KEY = ? END";

	}

	@Override
	List<String> getColNames() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getColNames()");

		List<String> ret = new ArrayList<>();
		ret.add(EVENT_NAME);
		ret.add(EVENT_ID);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getColNames()");

		return ret;
	}

}
