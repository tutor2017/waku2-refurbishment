package com.humansystem.waku2.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKMNAG01_DF_05
 *
 * create-date 02/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
public class Wkmnag01Df05Dao extends BaseUpdateDao {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag01Df05Dao.class);

	@Override
	String getSQL() {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "update T_EVENT set EVENT_NAME = ?, EVENT_MEMO = ?, APPL_DEADLINE = ?, EVENT_MNGR_MAIL_ADDR = ?, EVENT_PASSWORD = ? where EVENT_ID = ?";
	}
}
