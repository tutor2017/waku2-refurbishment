package com.humansystem.waku2.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKMNAG02_DF_02
 *
 * create-date 21/05/2017.
 *
 * @author 森田
 * review   石井
 */

public class Wkmnag02Df02Dao extends BaseSelectDao{

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag02Df02Dao.class);
	public static final String APPL_MAIL_ADDR = "APPL_MAIL_ADDR";

	@Override
	String getSQL() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "SELECT TEA.APPL_MAIL_ADDR FROM T_EVENT_ATTEND TEA INNER JOIN T_EVENT_DATE TED "
				+ "ON TEA.EVENT_ID = TED.EVENT_ID "
				+ "AND TED.SORT_NO = 0 "
				+ "INNER JOIN T_EVENT_ATTEND_DATE TEAD "
				+ "ON TEA.EVENT_ATTEND_ID = TEAD.EVENT_ATTEND_ID AND TED.EVENT_DATE_ID = TEAD.EVENT_DATE_ID "
				+ "WHERE TEA.EVENT_ID = ? AND TED.DELETE_FLG = 0";
	}

	@Override
	List<String> getColNames() {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getColNames()");

		List<String> ret = new ArrayList<>();
		ret.add(APPL_MAIL_ADDR);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getColNames()");

		return ret;
	}

}
