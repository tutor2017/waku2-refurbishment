package com.humansystem.waku2.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKMNAG02_DF_02
 *
 * create-date 21/05/2017.
 *
 * @author 作成者 石井 修正者 森川
 */

public class Wkinpt01Df04Dao extends BaseSelectDao{
	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkinpt01Df04Dao.class);

	public static final String EVENT_ATTEND_ID = "EVENT_ATTEND_ID"; // イベント出欠ID
	public static final String APPL_NAME ="APPL_NAME";			// 申込者名
	public static final String APPL_COMMENT = "APPL_COMMENT";	// 申込者コメント
	public static final String APPL_MAIL_ADDR = "APPL_MAIL_ADDR";		// 申込者メールアドレス

	@Override
	List<String> getColNames() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getColNames()");

		List<String> ret = new ArrayList<>();
		ret.add(EVENT_ATTEND_ID);
		ret.add(APPL_NAME);
		ret.add(APPL_COMMENT);
		ret.add(APPL_MAIL_ADDR);
		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getColNames()");

		return ret;
	}

	@Override
	String getSQL() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "SELECT EVENT_ATTEND_ID, APPL_NAME, APPL_COMMENT, APPL_MAIL_ADDR FROM T_EVENT_ATTEND WHERE EVENT_ID = ? ";
	}
}