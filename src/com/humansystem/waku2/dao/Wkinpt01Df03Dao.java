package com.humansystem.waku2.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKINPT01_DF_03
 *
 * create-date 09/06/2017.
 *
 * @author 作成者:ishii_y 修正者:morita_k
 */
public class Wkinpt01Df03Dao extends BaseSelectDao {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkinpt01Df03Dao.class);

	/** Field */
	public static final String EVENT_DATE_ID = "EVENT_DATE_ID";
	public static final String APPL_TYPE = "APPL_TYPE"; // 申込区分
	public static final String EVENT_ATTEND_ID = "EVENT_ATTEND_ID"; // イベント出欠ID

	@Override
	List<String> getColNames() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getColNames()");

		List<String> ret = new ArrayList<>();
		ret.add(EVENT_DATE_ID);
		ret.add(APPL_TYPE);
		ret.add(EVENT_ATTEND_ID);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getColNames()");

		return ret;
	}

	@Override
	String getSQL() {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0005, "getSQL()");

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0006, "getSQL()");

		return "SELECT TEAD.EVENT_DATE_ID, TEAD.APPL_TYPE, TEAD.EVENT_ATTEND_ID " + "FROM T_EVENT_ATTEND_DATE TEAD "
				+ "INNER JOIN T_EVENT_DATE TED " + "ON TEAD.EVENT_DATE_ID = TED.EVENT_DATE_ID "
				+ "AND TED.DELETE_FLG = 0 " + "WHERE TED.EVENT_ID = ?";
	}
}