package com.humansystem.waku2.exception;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * Waku2RuntimeException
 * 内部内でおきた例外の親クラス
 *
 * create-date 27/04/2017.
 * @author HumanSystemCo.,Ltd.
 */
public class Waku2RuntimeException extends RuntimeException {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8615621185066670655L;

	/**
	 * ラップしたException.
	 */
	private Throwable rootCause;

	/**
	 * Waku2RuntimeException のコンストラクタ.
	 * @param message メッセージ
	 */
	public Waku2RuntimeException(String message) {
		super(message);
	}

	/**
	 * Waku2RuntimeException のコンストラクタ.
	 * @param message メッセージ
	 * @param ex ラップする例外オブジェクト
	 */
	public Waku2RuntimeException(String message, Throwable ex) {
		super(message);
		this.rootCause = ex;
	}

	/**
	 * ラップした例外オブジェクトを取得する.
	 * @return Throwable ラップした例外オブジェクト
	 */
	public Throwable getRootCause() {
		return rootCause;
	}

	/**
	 * 例外メッセージを取得する.
	 * @return String 例外メッセージ
	 */
	public String getMessage() {
		if (rootCause == null)
			return super.getMessage();
		else
			return super.getMessage() + "; nested exception is: \n\t" + rootCause.toString();
	}

	/**
	 * スタックトレースを出力する(PrintStream).
	 * @param printStream PrintStream
	 */
	public void printStackTrace(PrintStream printStream) {

		if (rootCause == null) {
			super.printStackTrace(printStream);
		} else {
			printStream.println(this);
			rootCause.printStackTrace(printStream);
		} // else

	}

	/**
	 * スタックトレースを出力する(PrintWriter).
	 * @param printWriter PrintWriter
	 */
	public void printStackTrace(PrintWriter printWriter) {

		if (rootCause == null) {
			super.printStackTrace(printWriter);
		} else {
			printWriter.println(this);
			rootCause.printStackTrace(printWriter);
		}

	}

	/**
	 * スタックトレースを出力する.
	 */
	public void printStackTrace() {
		printStackTrace(System.err);
		// printStackTrace(System.out);
	}
}
