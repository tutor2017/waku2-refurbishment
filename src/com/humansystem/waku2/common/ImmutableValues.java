package com.humansystem.waku2.common;

import java.lang.reflect.Field;
import java.text.MessageFormat;

import com.humansystem.waku2.exception.Waku2RuntimeException;

/**
 * 定数を定義するクラス
 *
 * create-date 27/04/2017.
 * @author HumanSystemCo.,Ltd.
 */
public class ImmutableValues {

	//***************************************************************************
	//*                    定数定義                                             *
	//***************************************************************************
	/** 当システムのURL */
	public static final String SITE_URL = "http://localhost:8080";

	/** 管理者ログイン画面のURI */
	public static final String MANAGER_LOGIN_URI = "/waku2/login/WKLGIN01.html";

	/** ワクワク調整表画面のURI */
	public static final String EVENT_DETAIL_URI = "/waku2/input/WKINPT01.html";

	/** 管理者ログイン画面のお知らせ用のメールのFromメールアドレス */
	public static final String MAIL_TO_MANAGER_FROM_ADDRESS = "waku2@humansystem.com";

	/** 管理者ログイン画面のお知らせ用のメールの件名 */
	public static final String MAIL_TO_MANAGER_SUBJECT = "調整丸からのお知らせ";

	/** メール送信サーバのIP */
	public static final String MAIL_SMTP_HOST = "localhost";

	/** メール送信サーバのPORT */
	public static final int MAIL_SMTP_PORT = 25;

	/** メール送信サーバのAUTH　（true / false） */
	public static final boolean MAIL_SMTP_AUTH = true;

	/** メール送信サーバの通信でTLSを使うか　（true / false） */
	public static final boolean MAIL_SMTP_STARTTLS_ENABLE = true;

	/** メール送信サーバの通信の接続するまでのタイムアウト値　（ミリ秒） */
	public static final int MAIL_SMTP_CONNECTIONTIMEOUT = 10000;

	/** メール送信サーバの通信の通信開始から終了までのタイムアウト値　（ミリ秒） */
	public static final int MAIL_SMTP_TIMEOUT = 10000;

	/** メールのデバッグログ出力を行うか　（true / false） */
	public static final boolean MAIL_DEBUG = true;

	/** 暗号化/復号化する際のキー　（HEXストリングで32文字） */
	public static final String CRYPTO_KEY = "4068756D616E73797374656D2E636F6D";

	/** ハッシュ化するに当たって対象文字列に付加する文字列を定義する */
	public static final String DIGEST_SALT = "waku2tyouseimaru";

	/** エラーメッセージ一覧にて定義されているエラーメッセージ */
	public static final String MSGERR0001 = "{0}は必須項目です。";
	public static final String MSGERR0002 = "{0}は{1}文字以内でご記入ください。";
	public static final String MSGERR0003 = "{0}は{1}文字以上でご記入ください。";
	public static final String MSGERR0004 = "{0}は{1}桁でご記入ください。";
	public static final String MSGERR0005 = "{0}は半角でご記入ください。";
	public static final String MSGERR0006 = "{0}は全角でご記入ください。";
	public static final String MSGERR0007 = "{0}は数字でご記入ください。";
	public static final String MSGERR0008 = "{0}は半角英数でご記入ください。";
	public static final String MSGERR0009 = "{0}は半角数字でご記入ください。";
	public static final String MSGERR0010 = "{0}は半角英字でご記入ください。";
	public static final String MSGERR0011 = "{0}は正しい日付をご記入下さい。";
	public static final String MSGERR0012 = "{0}は全角でご記入ください。";
	public static final String MSGERR0013 = "{0}は{1}に入力した場合には必須です。";
	public static final String MSGERR0014 = "{0}は{1}以降の日付を指定して下さい。";
	public static final String MSGERR0015 = "{0}は全角カナでご記入ください。";
	public static final String MSGERR0016 = "{0}を選択してください。";
	public static final String MSGERR0017 = "{0}の形式が正しくありません。";
	public static final String MSGERR0018 = "{0}は1行あたり{1}文字以内でご記入ください。";
	public static final String MSGERR0019 = "{0}は重複しないように入力してください。";
	public static final String MSGERR0020 = "{0}または{1}が正しくありません。";
	public static final String MSGERR0021 = "{0}を過ぎた為、受付できません。";
	public static final String MSGFTL0001 = "暗号化処理に失敗しました。";
	public static final String MSGFTL0002 = "復号化処理に失敗しました。";
	public static final String MSGFTL0003 = "ハッシュ化に失敗しました。";
	public static final String MSGFTL0004 = "定義されていないkey（{0}）が指定されました。";
	public static final String MSGFTL0005 = "データベースの{0}に失敗しました。";
	public static final String MSGFTL0006 = "SQLの実行に失敗しました。";
	public static final String MSGFTL0007 = "DBのDriverクラスのロードに失敗しました。";
	public static final String MSGFTL0008 = "データの取得に失敗しました。";
	public static final String MSGLOG0001 = "Start Servlet {}";
	public static final String MSGLOG0002 = "End Servlet {}";
	public static final String MSGLOG0003 = "Start Action {}";
	public static final String MSGLOG0004 = "End Action {}";
	public static final String MSGLOG0005 = "Start Dao {}";
	public static final String MSGLOG0006 = "End Dao {}";
	public static final String MSGLOG0007 = "Start {}";
	public static final String MSGLOG0008 = "End {}";


	/*====================================================================
	 *  アクセッサメソッド
	 ====================================================================*/
	/**
	 * keyに対する定義値を返却する
	 *
	 * @param key
	 * @return
	 * @throws Waku2RuntimeException
	 */
	public static String get(String key) throws Waku2RuntimeException{

		ImmutableValues obj = new ImmutableValues();
		String ret = null;
		Field fld = null;
		try {
			fld = obj.getClass().getDeclaredField(key);
			ret = (String)fld.get(obj);
		} catch (IllegalArgumentException e) {
			throw new Waku2RuntimeException(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			throw new Waku2RuntimeException(e.getMessage(), e);
		} catch (NoSuchFieldException e) {
			throw new Waku2RuntimeException(e.getMessage(), e);
		} catch (SecurityException e) {
			throw new Waku2RuntimeException(e.getMessage(), e);
		}

		return ret;
	}


	/**
	 * objsを定義値(String)に埋め込んだ文字列を返却する
	 *
	 * @param key
	 * @param objs
	 * @return
	 * @throws Waku2RuntimeException
	 */
	public static String format(String key, Object[] objs) throws Waku2RuntimeException{

		Object msg = get(key);

		if(msg instanceof String){
			return MessageFormat.format((String)msg, objs);
		}

		return null;
	}

}
