package com.humansystem.waku2.util;

import java.util.ArrayList;
import java.util.List;

import com.humansystem.waku2.common.ImmutableValues;

/**
 * イベント関連の各種処理を行うクラス
 *
 * create-date 27/04/2017.
 * @author HumanSystem Co.,Ltd.
 */
public class EventUtils {


	/**
	 * 候補日のTextAreaの入力チェックを行う
	 *
	 * @param text
	 * @return 正常時：NULL　／　エラー時：エラーメッセージID
	 */
	public static String validateEventDateText(String text){

		// 必須チェック
		if(text == null){
			return ImmutableValues.format("MSGERR0001",new String[]{"候補日"});
		}

		// 候補日リストを取得
		List<String> ary = splitEventDateText(text);

		// 候補日ごとのチェック
		List<String> tmp = new ArrayList<String>();
		for(String str : ary){

			// 文字巣数チェック
			if(str.length() > 100){
				return ImmutableValues.format("MSGERR0002",new String[]{"候補日","100"});
			}

			// 重複チェック
			if(tmp.contains(str)){
				return ImmutableValues.format("MSGERR0019",new String[]{"候補日"});
			} else {
				tmp.add(str);
			}

		}

		// 正常終了時NULLを返却
		return null;
	}


	/**
	 * 候補日のTextAreaから候補日テキストのListを生成する
	 *
	 * @param text
	 * @return
	 */
	public static List<String> splitEventDateText(String text){

		List<String> ret = new ArrayList<String>();

		if(text == null || "".equals(text)){
			return ret;
		}

		// 改行コード(\n)で分割
		String[] str = text.split("\n");

		// 空文字を除外
		for(int i = 0; i < str.length; i++){
			if(!"".equals(str[i])){
				ret.add(str[i]);
			}
		}

		return ret;
	}


	/**
	 * 管理者ログインのURLを生成する
	 *
	 * @param eventKey
	 * @return
	 */
	public static String getManagerLoginURL(String eventKey){
		return ImmutableValues.SITE_URL + ImmutableValues.MANAGER_LOGIN_URI + "?ek=" + eventKey;
	}


	/**
	 * ワクワク調整表のURLを生成する
	 *
	 * @param eventKey
	 * @param mailAddress
	 * @return
	 */
	public static String getEventDetailURL(String eventKey,String mailAddress) {

		if(mailAddress == null){
			return ImmutableValues.SITE_URL + ImmutableValues.EVENT_DETAIL_URI + "?ek=" + eventKey;
		} else {
			return ImmutableValues.SITE_URL + ImmutableValues.EVENT_DETAIL_URI + "?ek=" + eventKey + "&m=" + CryptoUtils.encrypto(mailAddress);
		}
	}


	/**
	 * ワクワク調整表のURLを生成する
	 *
	 * @param eventKey
	 * @return
	 */
	public static String getEventDetailURL(String eventKey) {
		return getEventDetailURL(eventKey,null);
	}


}
