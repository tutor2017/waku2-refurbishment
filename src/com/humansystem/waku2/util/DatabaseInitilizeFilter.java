package com.humansystem.waku2.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class DatabaseInitilizeFilter implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
//		DBUtils.DB_FILE_PATH=arg0.getServletContext().getRealPath("/") + "WEB-INF/db";
		Connection con=null;

		try {
			con = DBUtils.getConnection();
			if(existsTables(con)==false){
				createTables(con);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(con!=null){
				try {con.close();} catch (SQLException e) {}
				con=null;
			}
		}


	}

	public boolean existsTables(Connection con){

		boolean exists=false;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(CHECK_SQL);
			while(rs.next()){
				if(rs.getInt(1)>0){
					exists=true;
				}
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(rs!=null){
				try {rs.close();} catch (SQLException e) {}
				rs=null;
			}
			if(stmt!=null){
				try {stmt.close();} catch (SQLException e) {}
				stmt=null;
			}
		}

		return exists;
	}

	public boolean createTables(Connection con){
		Statement stmt = null;

		try {
			stmt = con.createStatement();
			for(String sql : INITIALIZE_SQLS){
				stmt.executeUpdate(sql);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(stmt!=null){
				try {stmt.close();} catch (SQLException e) {}
				stmt=null;
			}
		}

		return true;
	}


	private static String CHECK_SQL="select count(1) from information_schema.tables where table_type = 'TABLE'";
	private static String[] INITIALIZE_SQLS = new String[]{
			"create table T_EVENT ("+
					"  EVENT_ID INT AUTO_INCREMENT comment 'イベントID'"+
					"  , EVENT_KEY VARCHAR(256) not null comment 'イベントキー'"+
					"  , EVENT_NAME VARCHAR(300) not null comment 'イベント名'"+
					"  , APPL_DEADLINE DATE comment '申込締切日:現在日付<(本カラム＋１日)であれば申込可能.nullのときは常に申込可能'"+
					"  , EVENT_MEMO VARCHAR(4000) comment 'イベントメモ'"+
					"  , EVENT_MNGR_MAIL_ADDR VARCHAR(4000) comment 'イベント管理者メアド:カンマ区切りでメアドを格納'"+
					"  , EVENT_PASSWORD VARCHAR(4000) comment 'イベントパスワード:SHA256でハッシュ化したパスワードを格納する'"+
					"  , CNTCT_FLG INT not null comment '参加メンバー連絡フラグ:0：連絡しない、1：連絡する'"+
					"  , CNTCT_MAIL_BODY VARCHAR(4000) comment '連絡メール本文'"+
					"  , CNTCT_MAIL_ADDR VARCHAR(4000) comment '参加メンバー連絡メアド:カンマ区切りでメアドを格納'"+
					"  , constraint PK_T_EVENT primary key (EVENT_ID)"+
					") comment='イベント情報:' "
			,"create index IDX_T_EVENT_01"+
					"  on T_EVENT(EVENT_KEY)"
			,"create table T_EVENT_DATE ("+
					"  EVENT_DATE_ID INT AUTO_INCREMENT comment 'イベント日程ID'"+
					"  , EVENT_ID INT not null comment 'イベントID'"+
					"  , EVENT_DATE_TEXT VARCHAR(300) not null comment 'イベント日程テキスト:同一イベントIDのレコードに対して一意とする'"+
					"  , SORT_NO INT not null comment '表示順:1〜'"+
					"  , DELETE_FLG INT not null comment '削除フラグ:管理者ログイン後に、該当する日程を削除した場合に1がセットされる。1の場合は、日程一覧上から表示されなくなる。"+
					"0:未削除、1:削除'"+
					"  , constraint PK_T_EVENT_DATE primary key (EVENT_DATE_ID)"+
					") comment='イベント日程:' "
			,"create index IDX_T_EVENT_DATE_01"+
					"  on T_EVENT_DATE(EVENT_ID)"
			,"create index IDX_T_EVENT_DATE_02"+
					"  on T_EVENT_DATE(EVENT_DATE_TEXT)"
			,"create table T_EVENT_ATTEND ("+
					"  EVENT_ATTEND_ID INT AUTO_INCREMENT comment 'イベント出欠ID'"+
					"  , EVENT_ID INT not null comment 'イベントID'"+
					"  , APPL_NAME VARCHAR(150) not null comment '申込者名'"+
					"  , APPL_COMMENT VARCHAR(1500) comment '申込者コメント'"+
					"  , APPL_MAIL_ADDR VARCHAR(256) comment '申込者メアド:参加メンバ連絡を行った場合のみ自動でセットする（画面入力ではなくメール本分のURLに埋め込まれている）'"+
					"  , constraint PK_T_EVENT_ATTEND primary key (EVENT_ATTEND_ID)"+
					") comment='イベント出欠:' "
			,"create index IDX_T_EVENT_ATTEND_01"+
					"  on T_EVENT_ATTEND(EVENT_ID)"
			,"create table T_EVENT_ATTEND_DATE ("+
					"  EVENT_DATE_ID INT not null comment 'イベント日程ID'"+
					"  , EVENT_ATTEND_ID INT not null comment 'イベント出欠ID'"+
					"  , APPL_TYPE INT not null comment '申込区分:1:○、2:△、3:×'"+
					"  , constraint PK_T_EVENT_ATTEND_DATE primary key (EVENT_DATE_ID,EVENT_ATTEND_ID)"+
					") comment='イベント出欠日程:' "
	};


}
