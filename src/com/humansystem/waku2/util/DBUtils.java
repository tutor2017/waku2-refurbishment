package com.humansystem.waku2.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.exception.Waku2RuntimeException;

/**
 * DBのユーティリティクラス
 *
 * @author HumanSystemCo.,Ltd.
 */
public class DBUtils {

	static String DB_FILE_PATH = "~";

	static {
		try {
			Class.forName("org.h2.Driver");
		} catch (ClassNotFoundException e) {
			throw new Waku2RuntimeException(ImmutableValues.format("MSGFTL0007", null), e);
		}
	}

	/**
	 * コネクションの取得
	 *
	 * @return Connection
	 */
	public static Connection getConnection() {

		Connection conn = null;

		try {
			conn = DriverManager.getConnection("jdbc:h2:"+DB_FILE_PATH+"/waku2db", "sa", "");
		} catch (SQLException e) {
			throw new Waku2RuntimeException(
					ImmutableValues.format("MSGFTL0005", new String[] { "接続" }), e);
		}
		return conn;
	}

	/**
	 * コネクションのクローズ
	 *
	 * @param conn
	 */
	public static void close(Connection conn) {

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				throw new Waku2RuntimeException(
						ImmutableValues.format("MSGFTL0005", new String[] { "クローズ" }), e);
			} finally {
				conn = null;
			}
		}

	}



}
