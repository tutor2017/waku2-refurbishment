package com.humansystem.waku2.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.exception.Waku2RuntimeException;

/**
 * ハッシュ化を行うクラス
 *
 * create-date 27/04/2017.
 * @author HumanSystemCo.,Ltd.
 */
public class DigestUtils {

	/**
	 * SHA-256を使用してハッシュ化を行う
	 *
	 * @param text
	 * @return
	 */
	public static String digest(String text) {

        byte[] cipher_byte;

        MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			throw new Waku2RuntimeException(ImmutableValues.format("MSGFTL0003", null), e);
		}

		// ダイジェストの更新
        md.update((text+ImmutableValues.DIGEST_SALT).getBytes());

        // 計算結果を取得
        cipher_byte = md.digest();

        // Hex文字列に変換して返却
        return DatatypeConverter.printHexBinary(cipher_byte);
	}

}
