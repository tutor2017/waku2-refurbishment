package com.humansystem.waku2.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.exception.Waku2RuntimeException;

/**
 * メール関連の各種処理を行うクラス
 *
 * create-date 27/04/2017.
 *
 * @author HumanSystem Co.,Ltd.
 */
public class MailUtils {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(MailUtils.class);

	/**
	 * 管理者ログイン画面の通知メールの送信を行う
	 *
	 * @param mailAddrText
	 * @param eventKey
	 * @param mailBody
	 * @param password
	 */
	public static void sendMailToManager(String mailAddrText, String eventKey, String mailBody, String password) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "sendMailToManager()");

		// 管理者用ＵＲＬ
		String url = EventUtils.getManagerLoginURL(eventKey);
		StringBuffer body = new StringBuffer();

		// 本文作成
		body.append(mailBody);
		body.append("\n");
		body.append("------------------------------");
		body.append("\n");
		body.append("ログインはこちらから→　" + url);
		body.append("\n");
		body.append("パスワード " + password);
		body.append("\n");
		body.append("------------------------------");
		body.append("\n");

		// カンマ(,)で分割
		String[] mailAddr = mailAddrText.split(",");

		// 空文字を除外
		for (int i = 0; i < mailAddr.length; i++) {
			sendMail(mailAddr[i], body.toString());
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0008, "sendMailToManager()");
	}

	/**
	 * 参加候補者に対してワクワク調整票画面の通知メールの送信を行う
	 *
	 * @param mailAddrText
	 * @param eventKey
	 * @param mailBody
	 * @param applDeadlineText
	 */
	public static void sendMailToCand(String mailAddrText, String eventKey, String mailBody, String applDeadlineText) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0007, "sendMailToCand()");

		// カンマ(,)で分割
		String[] mailAddr = mailAddrText.split(",");

		// 空文字を除外
		for (int i = 0; i < mailAddr.length; i++) {
			// 管理者用ＵＲＬ
			String url = EventUtils.getEventDetailURL(eventKey, mailAddr[i]);

			// 本文作成
			StringBuffer body = new StringBuffer();
			body.append(mailBody);
			body.append("\n");
			body.append("------------------------------");
			body.append("\n");
			body.append("回答はこちらから→　" + url);
			body.append("\n");
			body.append("申込期限 " + applDeadlineText);
			body.append("\n");
			body.append("------------------------------");
			body.append("\n");

			sendMail(mailAddr[i], body.toString());
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0008, "sendMailToCand()");

	}

	/**
	 * メール送信本体
	 *
	 * @param mailAddrTo
	 */
	private static void sendMail(String mailAddrTo, String body) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0007, "sendMail()");

		Properties props = new Properties();

		// smptサーバに関する設定
		props.setProperty("mail.smtp.host", ImmutableValues.MAIL_SMTP_HOST);
		props.setProperty("mail.smtp.port", String.valueOf(ImmutableValues.MAIL_SMTP_PORT));
		props.setProperty("mail.smtp.auth", String.valueOf(ImmutableValues.MAIL_SMTP_AUTH));

		// タイムアウト値
		props.setProperty("mail.smtp.connectiontimeout", String.valueOf(ImmutableValues.MAIL_SMTP_CONNECTIONTIMEOUT));
		props.setProperty("mail.smtp.timeout", String.valueOf(ImmutableValues.MAIL_SMTP_TIMEOUT));

		// ユーザ名
		props.setProperty("mail.user", "HumanSystem Co.,Ltd.");
		// ホスト名
		props.setProperty("mail.host", ImmutableValues.MAIL_SMTP_HOST);

		// サーバとの会話内容を出力
		if (ImmutableValues.MAIL_DEBUG) {
			props.setProperty("mail.debug", String.valueOf(ImmutableValues.MAIL_DEBUG));
		}

		// パスワード認証つきのセッションを作成
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("Gmailのユーザ名", "Gmailのパスワード");
			}
		});

		MimeMessage msg = new MimeMessage(session);

		try {

			// 送信元
			msg.setFrom(new InternetAddress(ImmutableValues.MAIL_TO_MANAGER_FROM_ADDRESS));
			msg.setSender(new InternetAddress(ImmutableValues.MAIL_TO_MANAGER_FROM_ADDRESS));

			// TOを設定
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(mailAddrTo));

			// 件名
			msg.setSubject(ImmutableValues.MAIL_TO_MANAGER_SUBJECT, "ISO-2022-JP");
			// 本文
			msg.setText(body, "ISO-2022-JP");

			// 送信
			Transport.send(msg);

		} catch (AddressException e) {
			throw new Waku2RuntimeException(e.getMessage(), e);
		} catch (MessagingException e) {
			throw new Waku2RuntimeException(e.getMessage(), e);
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0008, "sendMail()");
	}

}
