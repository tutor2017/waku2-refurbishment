package com.humansystem.waku2.util;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.exception.Waku2RuntimeException;

/**
 * 暗号化／復号化を行うクラス
 *
 * create-date 27/04/2017.
 *
 * @author HumanSystem Co.,Ltd.
 */
public class CryptoUtils {

	/**
	 * 暗号化する。
	 *
	 * @param text
	 * @return
	 */
	public static String encrypto(String text) {

		// 実体呼び出し
		byte[] bytesEncrypt = encrypto(new String(DatatypeConverter.parseHexBinary(ImmutableValues.CRYPTO_KEY)), text);

		// 返却された暗号化済みbyte[]をHex文字列にして返却
		return DatatypeConverter.printHexBinary(bytesEncrypt);
	}

	/**
	 * 復号化する。
	 *
	 * @param text
	 * @return
	 */
	public static String decrypto(String text) {

		// 暗号化されたHex文字列をbyte[]に変換
		byte[] bytesText = DatatypeConverter.parseHexBinary(text);

		// 実体呼び出し
		byte[] bytesDecrypt = decrypto(new String(DatatypeConverter.parseHexBinary(ImmutableValues.CRYPTO_KEY)),
				bytesText);

		// 返却された復号化済みbyte[]をHex文字列にして返却
		return new String(bytesDecrypt);
	}

	/**
	 * 暗号化実体
	 *
	 * @param key
	 * @param text
	 * @return
	 */
	private static byte[] encrypto(String key, String text) {

		byte[] ret = null;
		SecretKeySpec sksSpec = new SecretKeySpec(key.getBytes(), "AES");

		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(javax.crypto.Cipher.ENCRYPT_MODE, sksSpec);
			ret = cipher.doFinal(text.getBytes());
		} catch (Exception e) {
			throw new Waku2RuntimeException(ImmutableValues.format("MSGFTL0001", null), e);
		}
		return ret;
	}

	/**
	 * 復号化実体
	 *
	 * @param key
	 * @param encrypted
	 * @return
	 */
	private static byte[] decrypto(String key, byte[] encrypted) {

		byte[] ret = null;
		SecretKeySpec sksSpec = new SecretKeySpec(key.getBytes(), "AES");

		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, sksSpec);
			ret = cipher.doFinal(encrypted);
		} catch (Exception e) {
			throw new Waku2RuntimeException(ImmutableValues.format("MSGFTL0002", null), e);
		}
		return ret;
	}
}
