package com.humansystem.waku2.util;

public class StringUtils {

	/**
	 * Stringが空文字列もしくはnullであるかをチェックする
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str){
		if(str == null) {
			return true;
		}
		if("".equals(str)){
			return true;
		}
		return false;
	}
}
