package com.humansystem.waku2.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * WKINPT01/わくわく調整表のBeanクラス
 *
 * create-date 01/05/2017.
 *
 * @author 作成者:ishii_y 修正者:morita_k
 */
public class Wkinpt01Bean  extends BaseBean{

	// 〇
	private String maru = "";

	// ×
	private String batu = "";

	// △
	private String sankaku = "";

	// イベント名
	private String eventName = "";

	// メモ
	private String eventMemo = "";

	// イベント日程テキスト(候補案)
	private String eventDateText = "";

	// 締切日
	private String applDeadline = "";

	// 申込者メールアドレス
	private String eventApplAddr = "";

	// 表示名
	private String applName = "";

	// 暗号化イベントID
	private String cryptedEventId = "";

	// 暗号化イベント出欠ID
	private String cryptedEventAttendid = "";

	// イベントキー
	private String eventKey = "";

	// 参加者メンバー連絡フラグ
	private String cntctFlg = "";

	// 申込区コメント
	private String applComment = "";

	// 申込区分
	private String applType = "";

	//イベント日程ID
	private String eventDateID="";

	private String allMemberCount="";

	//まとめリスト 日程
	List<Wkinpt01Bean> eventDateList = new ArrayList<Wkinpt01Bean>();

	//まとめリスト 申込区分
	List<Wkinpt01Bean> eventDateList1 = new ArrayList<Wkinpt01Bean>();

	//まとめリスト 申込者名
	List<Wkinpt01Bean> eventDateList2 = new ArrayList<Wkinpt01Bean>();

	//まとめリスト 申込者コメント
	List<Wkinpt01Bean> eventDateList3 = new ArrayList<Wkinpt01Bean>();

	/**
	 * 通常はJSPのパスを返却する。 WebContentからの相対パスで記述 例）"/WEB-INF/jsp/test.jsp"
	 *
	 * @return
	 */
	public String getForwardPath() {
		return "/WEB-INF/jsp/Wkinpt01.jsp";
	}
	//イベ名
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	//メモ
	public String getEventMemo() {
		return eventMemo;
	}

	public void setEventMemo(String eventMemo) {
		this.eventMemo = eventMemo;
	}
	//候補
	public String getEventDateText() {
		return eventDateText;
	}

	public void setEventDateText(String eventDateText) {
		this.eventDateText = eventDateText;
	}

	public String getApplDeadline() {
		return applDeadline;
	}

		public void setApplDeadline(String applDeadline) {
		this.applDeadline = applDeadline;
	}

	public String getCryptedEventId() {
		return cryptedEventId;
	}

	public void setCryptedEventId(String cryptedEventId) {
		this.cryptedEventId = cryptedEventId;
	}
	// 申込者メールアドレス
	public String getEventApplAddr() {
		return eventApplAddr;
	}
	public void setEventApplAddr(String eventApplAddr) {
		this.eventApplAddr = eventApplAddr;
	}
	// 表示名
	public String getApplName() {
		return applName;
	}

	public void setApplName(String applName) {
		this.applName = applName;
	}
	// 暗号化イベントID
	public String getCryptedEventAttendid() {
		return cryptedEventAttendid;
	}

	public void setCryptedEventAttendid(String cryptedEventAttendid) {
		this.cryptedEventAttendid = cryptedEventAttendid;
	}
	//イベントキー
	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
	//フラグ
	public String getCntctFlg() {
		return cntctFlg;
	}

	public void setCntctFlg(String cntctFlg) {
		this.cntctFlg = cntctFlg;
	}
	// 申込区コメント
	public String getApplComment() {
		return applComment;
	}
	public void setApplComment(String applComment) {
		this.applComment = applComment;
	}
	// 申込区分
	public String getApplType() {
		return applType;
	}
	public void setApplType(String applType) {
		this.applType = applType;
	}

	public List<Wkinpt01Bean> getEventDateList() {
		return eventDateList;
	}

	public String getEventDateID() {
		return eventDateID;
	}
	public void setEventDateID(String eventDateID) {
		this.eventDateID = eventDateID;
	}
	public void setEventDateList(List<Wkinpt01Bean> eventDateList) {
		this.eventDateList = eventDateList;
	}

	public List<Wkinpt01Bean> getEventDateList2() {
		return eventDateList2;
	}
	public void setEventDateList2(List<Wkinpt01Bean> eventDateList2) {
		this.eventDateList2 = eventDateList2;
	}
	public List<Wkinpt01Bean> getEventDateList3() {
		return eventDateList3;
	}
	public void setEventDateList3(List<Wkinpt01Bean> eventDateList3) {
		this.eventDateList3 = eventDateList3;
	}
	public List<Wkinpt01Bean> getEventDateList1() {
		return eventDateList1;
	}
	public void setEventDateList1(List<Wkinpt01Bean> eventDateList1) {
		this.eventDateList1 = eventDateList1;
	}
	public String getMaruCount() {
		return maru;
	}
	public void setMaruCount(String maruCount) {
		this.maru = maruCount;
	}
	public String getBatuCount() {
		return batu;
	}
	public void setBatuCount(String batuCount) {
		this.batu = batuCount;
	}
	public String getSankakuCount() {
		return sankaku;
	}
	public void setSankakuCount(String sankakuCount) {
		this.sankaku = sankakuCount;
	}
	public String getAllMemberCount() {
		return allMemberCount;
	}
	public void setAllMemberCount(String allMemberCount) {
		this.allMemberCount = allMemberCount;
	}
}