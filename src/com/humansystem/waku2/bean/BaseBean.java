package com.humansystem.waku2.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Beanの親クラス
 *
 * create-date 27/04/2017.
 * @author HumanSystemCo.,Ltd.
 */
abstract public class BaseBean {

	/** リダイレクト先URLを格納する */
	private String redirectURL=null;

	/** エラー情報を格納する */
	private List<String> error = new ArrayList<>();

	/**
	 * @return redirectURL
	 */
	public String getRedirectURL() {
		return redirectURL;
	}

	/**
	 * @param redirectURL セットする redirectURL
	 */
	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

	/**
	 * 通常はJSPのパスを返却する。
	 * WebContentからの相対パスで記述
	 * 例）"/WEB-INF/jsp/test.jsp"
	 * @return
	 */
	abstract public String getForwardPath();

	/**
	 * @return error
	 */
	public List<String> getError() {
		return error;
	}

	/**
	 * エラーメッセージを追加します。
	 * @param errorStr
	 */
	public void addError(String errorStr) {
		error.add(errorStr);
	}

}