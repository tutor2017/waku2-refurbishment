package com.humansystem.waku2.bean;

/**
 * WKMNAG02/参加メンバーを募るのBeanクラス
 *
 * create-date 22/05/2017.
 *
 *
 */

public class Wkmnag02Bean extends BaseBean{

	// わくわく調整表URL
	private String chouseiUrl = "";

	// 参加メンバーへの連絡を行う
	private String memberContact = "";

	// メールテキスト
	private String mailText = "";

	// 管理者メールアドレス
	private String eventMngrMailAddr = "";

	// 参加者メールアドレス
	private String participantMailaddress = "";

	// 回答済みメールアドレス
	private String answeredMailaddress = "";

	// 全員にメールを送る
	private String allMembersSendMail = "";

	// 暗号化イベントID
	private String cryptedEventId = "";

	// 連絡変更可能フラグ
	private String contactChangeFlag = "";

	// イベントキー
	private String eventKey = "";

	// 締切日
	private String applDeadline = "";

	/**
	 * 通常はJSPのパスを返却する。 WebContentからの相対パスで記述 例）"/WEB-INF/jsp/test.jsp"
	 *
	 * @return
	 */
	public String getForwardPath() {
		return "/WEB-INF/jsp/Wkmnag02.jsp";
	}

	public String getChouseiUrl() {
		return chouseiUrl;
	}

	public void setChouseiUrl(String chouseiUrl) {
		this.chouseiUrl = chouseiUrl;
	}

	public String getMemberContact() {
		return memberContact;
	}

	public void setMemberContact(String memberContact) {
		this.memberContact = memberContact;
	}

	public String getMailText() {
		return mailText;
	}

	public void setMailText(String mailText) {
		this.mailText = mailText;
	}

	public String getEventMngrMailAddr() {
		return eventMngrMailAddr;
	}

	public void setEventMngrMailAddr(String eventMngrMailAddr) {
		this.eventMngrMailAddr = eventMngrMailAddr;
	}

	public String getParticipantMailaddress() {
		return participantMailaddress;
	}

	public void setParticipantMailaddress(String participantMailaddress) {
		this.participantMailaddress = participantMailaddress;
	}

	public String getAnsweredMailaddress() {
		return answeredMailaddress;
	}

	public void setAnsweredMailaddress(String answeredMailaddress) {
		this.answeredMailaddress = answeredMailaddress;
	}

	public String getAllMembersSendMail() {
		return allMembersSendMail;
	}

	public void setAllMembersSendMail(String allMembersSendMail) {
		this.allMembersSendMail = allMembersSendMail;
	}

	public String getCryptedEventId() {
		return cryptedEventId;
	}

	public void setCryptedEventId(String cryptedEventId) {
		this.cryptedEventId = cryptedEventId;
	}

	public String getContactChangeFlag() {
		return contactChangeFlag;
	}

	public void setContactChangeFlag(String contactChangeFlag) {
		this.contactChangeFlag = contactChangeFlag;
	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public String getApplDeadline() {
		return applDeadline;
	}

	public void setApplDeadline(String applDeadline) {
		this.applDeadline = applDeadline;
	}

}
