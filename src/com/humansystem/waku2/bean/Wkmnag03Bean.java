package com.humansystem.waku2.bean;

/**
 * WKINPT03/完了のBeanクラス
 *
 * create-date 21/05/2017.
 *
 *@author Morita_k / Mizobe_n
 */

public class Wkmnag03Bean extends BaseBean {

	// イベントキー
	private String eventKey = "";

	/**
	 * 通常はJSPのパスを返却する。 WebContentからの相対パスで記述 例）"/WEB-INF/jsp/test.jsp"
	 *
	 * @return
	 */
	@Override
	public String getForwardPath() {
		return "/WEB-INF/jsp/Wkmnag03.jsp";
	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

}
