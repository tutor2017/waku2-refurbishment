package com.humansystem.waku2.bean;

/**
 * WKLGIN01/管理者ログイン画面のBeanクラス
 *
 * create-date 18/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
public class Wklgin01Bean extends BaseBean{

	// イベント名
	private String eventName = "";

	// メモ
	private String eventMemo = "";

	// 候補
	private String eventDateText = "";

	// 締切日
	private String applDeadline = "";

	// 管理者メールアドレス
	private String eventMngrMailAddr = "";

	// イベントパスワード
	private String eventPassword = "";

	// 暗号化イベントID
	private String cryptedEventId = "";

	//イベントキー
	private String EventKey = "";

	/**
	 * 通常はJSPのパスを返却する。 WebContentからの相対パスで記述 例）"/WEB-INF/jsp/test.jsp"
	 *
	 * @return
	 */
	public String getForwardPath() {
		return "/WEB-INF/jsp/Wklgin01.jsp";
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventMemo() {
		return eventMemo;
	}

	public void setEventMemo(String eventMemo) {
		this.eventMemo = eventMemo;
	}

	public String getEventDateText() {
		return eventDateText;
	}

	public void setEventDateText(String eventDateText) {
		this.eventDateText = eventDateText;
	}

	public String getApplDeadline() {
		return applDeadline;
	}

	public void setApplDeadline(String applDeadline) {
		this.applDeadline = applDeadline;
	}

	public String getEventMngrMailAddr() {
		return eventMngrMailAddr;
	}

	public void setEventMngrMailAddr(String eventMngrMailAddr) {
		this.eventMngrMailAddr = eventMngrMailAddr;
	}

	public String getEventPassword() {
		return eventPassword;
	}

	public void setEventPassword(String eventPassword) {
		this.eventPassword = eventPassword;
	}

	public String getCryptedEventId() {
		return cryptedEventId;
	}

	public void setCryptedEventId(String cryptedEventId) {
		this.cryptedEventId = cryptedEventId;
	}

	public String getEventKey() {
		return EventKey;
	}

	public void setEventKey(String EventKey) {
		this.EventKey = EventKey;
	}

}
