package com.humansystem.waku2.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * WKinpt02/○△✕入力画面のBeanクラス
 *
 * create-date 26/05/2017.
 *
 * @author mizobe_n (revision:kasai_r)
 */

public class Wkinpt02Bean extends BaseBean{

	//候補
	private String eventDateText = "";

	//締切日
	private String applDeadline  = "";

	//イベントキー
	private String eventKey = "";

	//暗号化イベントID
	private String cryptedEventId = "";

	//暗号化イベント出欠ID
	private String cryptedEventAttendId = "";

	//暗号化イベント日程ID
	private String cryptedEventDateId = "";

	//表示名
	private String applName = "";

	//候補選択
	private String applType = "";

	// 申込者コメント
	private String applComment = "";

	// 申込者メールアドレス
	private String applMailAddr = "";

	// 候補日・入力情報リスト
	private List<Wkinpt02Bean> eventDateList = new ArrayList<Wkinpt02Bean>();

	public String getForwardPath(){
		return "/WEB-INF/jsp/Wkinpt02.jsp";
	}

	public String getEventKey(){
		return eventKey;
	}

	public void setEventKey(String eventKey){
		this.eventKey = eventKey;
	}

	public String getEventDateText(){
		return eventDateText;
	}

	public void setEventDateText(String eventDateText){
		this.eventDateText = eventDateText;
	}

	public String getEventApplDeadline(){
		return applDeadline;
	}

	public void setApplDeadline(String applDeadline){
		this.applDeadline = applDeadline;
	}

	public String getCryptedEventId(){
		return cryptedEventId;
	}

	public void setCryptedEventId(String cryptedEventId){
		this.cryptedEventId = cryptedEventId;
	}

	public String getCryptedEventAttendId(){
		return cryptedEventAttendId;
	}

	public void setCryptedEventAttendId(String cryptedEventAttendId){
		this.cryptedEventAttendId = cryptedEventAttendId;
	}

	public String getCryptedEventDateId(){
		return cryptedEventDateId;
	}

	public void setCryptedEventDateId(String cryptedEventDateId){
		this.cryptedEventDateId = cryptedEventDateId;
	}

	public String getApplName(){
		return applName;
	}

	public void setApplName(String applName){
		this.applName = applName;
	}

	public String getApplType(){
		return applType;
	}

	public void setApplType(String applType){
		this.applType = applType;
	}

	public String getApplComment(){
		return applComment;
	}

	public void setApplComment(String applComment){
		this.applComment = applComment;
	}

	public String getApplMailAddr(){
		return applMailAddr;
	}

	public void setApplMailAddr(String applMailAddr){
		this.applMailAddr = applMailAddr;
	}

	public List<Wkinpt02Bean> getEventDateList(){
		return eventDateList;
	}

	public void setEventDateList(List<Wkinpt02Bean> EventDateList){
		this.eventDateList = EventDateList;
	}
}

