package com.humansystem.waku2.servlet;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.action.BaseAction;
import com.humansystem.waku2.action.Wkinpt02Ev01Action;
import com.humansystem.waku2.action.Wkinpt02Ev02Action;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.util.StringUtils;


@WebServlet(name = "Wkinpt02", urlPatterns = { "/input/WKINPT02.html" })

/**
 * WKinpt02/○△✕入力画面のServletクラス
 *
 * create-date 20/05/2017.
 *
 * @author mizobe_n (revision:kasai_r)
 */

public class Wkinpt02Servlet extends BaseServlet{

	/**	serialVersionUID */
	private static final long serialVersionUID = 4844943742226475502L;

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkinpt02Servlet.class);

	/**
	 * <p>ボタン押下時　⇒　Wkinpt02Ev02Actionを呼ぶ</p>
	 * <p>何もなし　　　⇒　Wkinpt02Ev01Actionを呼ぶ</p>
	 */
	@Override
	BaseAction dispatch(HttpServletRequest req, HttpServletResponse resp){
		/** 開始ログ */
		log.info(ImmutableValues.MSGLOG0001, "dispatch()");

		// アクションクラス
		BaseAction action = null;

		if (StringUtils.isEmpty(req.getParameter("registButton")) == false) {
			// 出欠情報の登録・更新
			action = new Wkinpt02Ev02Action();

		} else {
			// 初期表示
			action = new Wkinpt02Ev01Action();

		}

		/**終了ログ */
		log.info(ImmutableValues.MSGLOG0002,"dispatch");

		return action;
	}



}
