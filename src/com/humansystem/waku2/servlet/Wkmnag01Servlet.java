package com.humansystem.waku2.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.action.BaseAction;
import com.humansystem.waku2.action.Wkmnag01Ev01Action;
import com.humansystem.waku2.action.Wkmnag01Ev02Action;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.util.StringUtils;

@WebServlet(name = "Wkmnag01", urlPatterns = { "/manage/WKMNAG01.html" })

/**
 * WKMNAG01/わくわく調整表作成画面のServletクラス
 *
 * create-date 29/04/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
public class Wkmnag01Servlet extends BaseServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = -3685650707196816826L;

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag01Servlet.class);

	/**
	 * <p>ボタン押下時　⇒　Wkmnag01Ev02Actionを呼ぶ</p>
	 * <p>何もなし　　　⇒　Wkmnag01Ev01Actionを呼ぶ</p>
	 */
	@Override
	BaseAction dispatch(HttpServletRequest req, HttpServletResponse resp) {

		/** 開始ログ */
		log.info(ImmutableValues.MSGLOG0001, "dispatch()");

		// アクションクラス
		BaseAction action = null;

		if (StringUtils.isEmpty(req.getParameter("createWaku2Button")) == false) {
			// わくわく調整表の作成
			action = new Wkmnag01Ev02Action();

		} else {
			// 初期表示
			action = new Wkmnag01Ev01Action();

		}

		/** 終了ログ */
		log.info(ImmutableValues.MSGLOG0002, "dispatch()");

		return action;
	}

}
