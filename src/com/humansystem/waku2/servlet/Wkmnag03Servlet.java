package com.humansystem.waku2.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.action.BaseAction;
import com.humansystem.waku2.action.Wkmnag03Ev01Action;
import com.humansystem.waku2.action.Wkmnag03Ev02Action;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.util.StringUtils;

@WebServlet(name = "Wkmnag03", urlPatterns = { "/manage/WKMNAG03.html" })

/**
 * WKMNAG03/完了画面のServletクラス
 *
 * create-date 24/05/2017.
 *
 */

public class Wkmnag03Servlet extends BaseServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = -3685650707196816826L;

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag03Servlet.class);

	/**
	 * <p>ボタン押下時　⇒　Wkmnag03Ev02Actionを呼ぶ</p>
	 * <p>何もなし　　　⇒　Wkmnag03Ev01Actionを呼ぶ</p>
	 */
	@Override
	BaseAction dispatch(HttpServletRequest req, HttpServletResponse resp) {
		/** 開始ログ */
		log.info(ImmutableValues.MSGLOG0001, "dispatch()");

		// アクションクラス
		BaseAction action = null;

		if (StringUtils.isEmpty(req.getParameter("goToLoginButton")) == false) {
			// WKLGIN01/管理者ログイン画面へ遷移する
			action = new Wkmnag03Ev02Action();

		} else {
			// 初期表示
			action = new Wkmnag03Ev01Action();

		}

		/** 終了ログ */
		log.info(ImmutableValues.MSGLOG0002, "dispatch()");

		return action;
	}

}
