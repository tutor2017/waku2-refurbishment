package com.humansystem.waku2.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.action.BaseAction;
import com.humansystem.waku2.action.Wkmnag02Ev01Action;
import com.humansystem.waku2.action.Wkmnag02Ev02Action;
import com.humansystem.waku2.action.Wkmnag02Ev03Action;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.util.StringUtils;

@WebServlet(name = "Wkmnag02", urlPatterns = { "/manage/WKMNAG02.html" })

/**
 * WKMNAG02/参加者募る画面のServletクラス
 *
 * create-date 21/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
public class Wkmnag02Servlet extends BaseServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = -3685650707196816826L;

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag02Servlet.class);

	/**
	 * <p>ボタン押下時(登録ボタン)　⇒　Wkmnag02Ev02Actionを呼ぶ</p>
	 * <p>ボタン押下時(コピーボタン)⇒　Wkmnag02Ev03Actionを呼ぶ</p>
	 * <p>何もなし　　　　　　　　　⇒　Wkmnag02Ev01Actionを呼ぶ</p>
	 */
	@Override
	BaseAction dispatch(HttpServletRequest req, HttpServletResponse resp) {

		/** 開始ログ */
		log.info(ImmutableValues.MSGLOG0001, "dispatch()");

		// アクションクラス
		BaseAction action = null;

		if (StringUtils.isEmpty(req.getParameter("registButton")) == false) {
			// 参加者を募る画面の登録処理
			action = new Wkmnag02Ev02Action();

		}else if(StringUtils.isEmpty(req.getParameter("copyUrl")) == false){

			action = new Wkmnag02Ev03Action();

		} else {
			// 初期表示
			action = new Wkmnag02Ev01Action();

		}

		/** 終了ログ */
		log.info(ImmutableValues.MSGLOG0002, "dispatch()");

		return action;
	}

}
