package com.humansystem.waku2.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.action.BaseAction;
import com.humansystem.waku2.action.Wklgin01Ev01Action;
import com.humansystem.waku2.action.Wklgin01Ev02Action;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.util.StringUtils;


@WebServlet(name = "Wklgin01", urlPatterns = {"/login/WKLGIN01.html"})

/**
 * WKLGIN01/管理者ログイン画面のServletクラス
 *
 * create-date 18/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
public class Wklgin01Servlet extends BaseServlet{

	private static Logger log = LoggerFactory.getLogger(Wklgin01Servlet.class);

	/**
	 * <p>ボタン押下時　⇒　Wklgin01Ev02Actionを呼ぶ</p>
	 * <p>何もなし　　　⇒　Wklgin01Ev01Actionを呼ぶ</p>
	 */
	@Override
	BaseAction dispatch(HttpServletRequest req, HttpServletResponse resp) {

		/** 開始ログ */
		log.info(ImmutableValues.MSGLOG0001, "dispatch()");

		// アクションクラス
		BaseAction action = null;

		if (StringUtils.isEmpty(req.getParameter("loginButton")) == false) {

			// ログイン認証処理
			action = new Wklgin01Ev02Action();

		} else {

			// 初期表示
			action = new Wklgin01Ev01Action();

		}

		/** 終了ログ */
		log.info(ImmutableValues.MSGLOG0002, "dispatch()");

		return action;
	}

}
