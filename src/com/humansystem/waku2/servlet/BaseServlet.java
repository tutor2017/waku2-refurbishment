package com.humansystem.waku2.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.action.BaseAction;
import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.util.DBUtils;
import com.humansystem.waku2.util.StringUtils;

/**
 * Servletの親クラス
 *
 * create-date 27/04/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
abstract class BaseServlet extends HttpServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = -9158436135326388587L;

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(BaseServlet.class);

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		/** 開始ログ */
		log.info(ImmutableValues.MSGLOG0001, "doPost()");

		doProcess(req, resp);

		/** 終了ログ */
		log.info(ImmutableValues.MSGLOG0002, "doPost()");

	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		/** 開始ログ */
		log.info(ImmutableValues.MSGLOG0001, "doGet()");

		doProcess(req, resp);

		/** 終了ログ */
		log.info(ImmutableValues.MSGLOG0002, "doGet()");

	}

	/**
	 * Servletのメイン処理
	 * @param req
	 * @param resp
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doProcess(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		/** 開始ログ */
		log.info(ImmutableValues.MSGLOG0001, "doProcess()");

		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");

		BaseAction action = dispatch(req, resp);
		BaseBean bean = null;

		Connection con=null;

		try {
			con = DBUtils.getConnection();
			bean = action.populate(req, resp,con);

			if (StringUtils.isEmpty(bean.getRedirectURL()) == false) {
				con.commit();
				redirect(req, resp, bean);
				return;
			}

			action.validate(req, resp, bean,con);

			if (StringUtils.isEmpty(bean.getRedirectURL()) == false) {
				con.commit();
				redirect(req, resp, bean);
				return;
			}

			action.process(req, resp, bean,con);

			if (StringUtils.isEmpty(bean.getRedirectURL()) == false) {
				con.commit();
				redirect(req, resp, bean);
				return;
			}

			con.commit();

		} catch (Exception e) {
			if(con != null){
				try {
					con.rollback();
				} catch (SQLException e1) {
					log.error(ImmutableValues.format("MSGFTL0005", new String[] { "ロールバック" }), e1);

				}
			}
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			log.error(e.getMessage(), e);
			return;
		} finally {
			if(con != null){
				DBUtils.close(con);
				con=null;
			}
		}

		render(req, resp, bean);

		/** 終了ログ */
		log.info(ImmutableValues.MSGLOG0002, "doProcess()");

	}

	/**
	 * 画面の初期表示なのか、何かボタンを押したのかの判断し、Actionクラスを戻す
	 */
	abstract BaseAction dispatch(HttpServletRequest req, HttpServletResponse resp);

	/**
	 * responseにリダイレクト設定を行う
	 *
	 * @param resp
	 * @param redirectUrl
	 * @throws IOException
	 */
	protected void redirect(HttpServletRequest req, HttpServletResponse resp, BaseBean bean) throws IOException {

		/** 開始ログ */
		log.info(ImmutableValues.MSGLOG0001, "redirect()");

		resp.sendRedirect( bean.getRedirectURL());

		/** 終了ログ */
		log.info(ImmutableValues.MSGLOG0002, "redirect()");

	}

	/**
	 * 画面のレンダリングを行う。 標準実装としては指定されたPathに対してForwardする
	 *
	 * @param req
	 * @param resp
	 * @param bean
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void render(HttpServletRequest req, HttpServletResponse resp, BaseBean bean)
			throws ServletException, IOException {

		/** 開始ログ */
		log.info(ImmutableValues.MSGLOG0001, "render()");

		req.setAttribute("__bean__", bean);
		req.getRequestDispatcher(bean.getForwardPath()).forward(req, resp);

		/** 終了ログ */
		log.info(ImmutableValues.MSGLOG0002, "render()");

	}
}
