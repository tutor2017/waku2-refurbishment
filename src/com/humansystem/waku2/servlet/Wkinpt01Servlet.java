package com.humansystem.waku2.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.action.BaseAction;
import com.humansystem.waku2.action.Wkinpt01Ev01Action;
import com.humansystem.waku2.action.Wkinpt01Ev02Action;
import com.humansystem.waku2.action.Wkinpt01Ev03Action;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.util.StringUtils;

/**
 * WKINPT01/わくわく調整表のBeanクラス
 *
 * create-date 01/05/2017.
 *
 * @author 作成者:ishii_y 修正者:morita_k
 */

@WebServlet(name = "Wkinpt01", urlPatterns = {"/input/WKINPT01.html"})
public class Wkinpt01Servlet extends  BaseServlet{
	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkinpt01Servlet.class);

	/**
	 * <p>リンク押下時　⇒　Wkinpt01Ev03Actionを呼ぶ</p>
	 * <p>ボタン押下時　⇒　Wkinpt01Ev02Actionを呼ぶ</p>
	 * <p>何もなし　　　⇒　Wkinpt01Ev01Actionを呼ぶ</p>
	 */
	@Override
	BaseAction dispatch(HttpServletRequest req, HttpServletResponse resp) {
		/** 開始ログ */
		log.info(ImmutableValues.MSGLOG0001, "dispatch()");

		// アクションクラス
		BaseAction action = null;


		// リンク
		if (StringUtils.isEmpty(req.getParameter("applName")) == false) {
			// 表示名処理
				action = new Wkinpt01Ev03Action();

		}// ボタン
		else if (StringUtils.isEmpty(req.getParameter("inputButton")) == false) {
			// 〇△×処理
				action = new Wkinpt01Ev02Action();

		} else {
			// 初期表示
				action = new Wkinpt01Ev01Action();

		}


		/** 終了ログ */
		log.info(ImmutableValues.MSGLOG0002, "dispatch()");

		return action;
	}

}