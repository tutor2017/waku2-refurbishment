package com.humansystem.waku2.action;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.bean.Wkmnag01Bean;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.dao.Wkmnag01Df01Dao;
import com.humansystem.waku2.dao.Wkmnag01Df03Dao;
import com.humansystem.waku2.dao.Wkmnag01Df04Dao;
import com.humansystem.waku2.dao.Wkmnag01Df05Dao;
import com.humansystem.waku2.dao.Wkmnag01Df06Dao;
import com.humansystem.waku2.dao.Wkmnag01Df07Dao;
import com.humansystem.waku2.exception.Waku2RuntimeException;
import com.humansystem.waku2.util.CryptoUtils;
import com.humansystem.waku2.util.MailUtils;
import com.humansystem.waku2.util.StringUtils;

/**
 * WKMNAG01/わくわく調整表のActionクラス
 * <p>
 * WKMNAG01_EV_02
 * <p/>
 *
 * 説明：わくわく調整表の作成を行う
 *
 * create-date 01/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
public class Wkmnag01Ev02Action extends BaseAction {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag01Ev02Action.class);

	/**
	 * リクエスト情報から、画面項目の情報を取得し、Beanに設定する
	 */
	@Override
	public BaseBean populate(HttpServletRequest req, HttpServletResponse resp, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "populate()");

		Wkmnag01Bean bean = (Wkmnag01Bean) req.getAttribute("__bean__");

		if (bean == null) {
			bean = new Wkmnag01Bean();
		}

		bean.setEventName(req.getParameter("eventName"));
		bean.setEventMemo(req.getParameter("eventMemo"));
		bean.setEventDateText(req.getParameter("eventDateText"));
		bean.setApplDeadline(req.getParameter("applDeadline"));
		bean.setEventMngrMailAddr(req.getParameter("eventMngrMailAddr"));
		bean.setEventPassword(req.getParameter("eventPassword"));
		bean.setCryptedEventId(req.getParameter("cryptedEventId"));

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "populate()");

		return bean;
	}

	/**
	 * 入力チェックを行う
	 */
	@Override
	public void validate(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "validate()");

		// イベント名
		String target = ((Wkmnag01Bean) obj).getEventName();
		String name = "イベント名";
		// 必須 最大
		if (StringUtils.isEmpty(target)) {
			obj.addError(ImmutableValues.format("MSGERR0001", new String[] { name }));
		} else {
			if (target.length() > 100) {
				obj.addError(ImmutableValues.format("MSGERR0002", new String[] { name, "100" }));
			}
			if (checkXSS(target)) {
				((Wkmnag01Bean) obj).setEventName("");
				obj.addError(ImmutableValues.format("MSGERR0017", new String[] { name }));
			}
		}

		// メモ
		target = ((Wkmnag01Bean) obj).getEventMemo();
		name = "メモ";
		// 最大
		if (target != null && target.length() > 1000) {
			obj.addError(ImmutableValues.format("MSGERR0002", new String[] { name, "1000" }));
		}
		if(target != null && checkXSS(target)) {
			((Wkmnag01Bean) obj).setEventMemo("");
			obj.addError(ImmutableValues.format("MSGERR0017", new String[] { name }));
		}

		// 候補
		target = ((Wkmnag01Bean) obj).getEventDateText();
		name = "候補";
		// 必須 個別1,2,6
		if (StringUtils.isEmpty(target)) {
			obj.addError(ImmutableValues.format("MSGERR0001", new String[] { name }));
		} else {
			String[] str = target.split("\r\n");
			List<String> lst = new ArrayList<String>();
			for (int i = 0; i < str.length; i++) {
				if (str[i] != null && str[i].length() > 100) {
					obj.addError(ImmutableValues.format("MSGERR0018", new String[] { name, "100" }));
				}
				if (lst.contains(str[i])) {
					obj.addError(ImmutableValues.format("MSGERR0019", new String[] { name }));
				} else {
					lst.add(str[i]);
				}

				//2017/06/28 変更分
				if(checkrn(str[i]) == true){
					obj.addError(ImmutableValues.format("MSGERR0017", new String[] { name }));
				}
				//ここまで

				if (checkXSS(str[i])) {
					((Wkmnag01Bean) obj).setEventDateText("");
					obj.addError(ImmutableValues.format("MSGERR0017", new String[] { name }));
				}
			}
		}

		// 締切日
		target = ((Wkmnag01Bean) obj).getApplDeadline();
		name = "締切日";
		// 個別3
		if (!checkDate(target)) {
			obj.addError(ImmutableValues.format("MSGERR0011", new String[] { name }));
		}

		// 管理者メールアドレス
		target = ((Wkmnag01Bean) obj).getEventMngrMailAddr();
		name = "管理者メールアドレス";
		boolean mlAddrFlg = false;
		// 最大 個別4
		if (target != null && target.length() > 4000) {
			obj.addError(ImmutableValues.format("MSGERR0002", new String[] { name, "4000" }));
		} else {
			if (StringUtils.isEmpty(target) == false) {
				mlAddrFlg = true;
				String[] str = target.split(",");
				for (int i = 0; i < str.length; i++) {
					if (!checkMlAddr(str[i])) {
						obj.addError(ImmutableValues.format("MSGERR0017", new String[] { name, "1000" }));
						break;
					}
				}
			}
		}

		// イベントパスワード
		target = ((Wkmnag01Bean) obj).getEventPassword();
		name = "イベントパスワード";
		// 最大 最小 半角 数字 英字 個別4
		if (StringUtils.isEmpty(target) == false && target.length() > 20) {
			obj.addError(ImmutableValues.format("MSGERR0002", new String[] { name, "20" }));
		}
		if (StringUtils.isEmpty(target) == false && target.length() < 10) {
			obj.addError(ImmutableValues.format("MSGERR0003", new String[] { name, "10" }));
		}
		if (!isHalfAlphNum(target)) {
			obj.addError(ImmutableValues.format("MSGERR0008", new String[] { name }));
		}
		// 個別5
		if (mlAddrFlg == true && StringUtils.isEmpty(target)) {
			obj.addError(ImmutableValues.format("MSGERR0013",
					new String[] { name, "管理者メールアドレス、イベントパスワード", "管理者メールアドレス、イベントパスワード" }));
		} else if (mlAddrFlg == false && StringUtils.isEmpty(target) == false) {
			obj.addError(ImmutableValues.format("MSGERR0013",
					new String[] { name, "管理者メールアドレス、イベントパスワード", "管理者メールアドレス、イベントパスワード" }));
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "validate()");

	}

	private boolean checkDate(String strDate) {
		if (strDate == null || strDate.length() != 10) {
			return false;
		}
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		format.setLenient(false);
		try {
			format.parse(strDate);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private boolean checkMlAddr(String address) {
		boolean result;
		String aText = "[\\w!#%&'/=~`\\*\\+\\?\\{\\}\\^\\$\\-\\|]";
		String dotAtom = aText + "+" + "(\\." + aText + "+)*";
		String regularExpression = "^" + dotAtom + "@" + dotAtom + "$";
		result = checkMailAddress(address, regularExpression);
		return result;
	}

	private boolean checkMailAddress(String address, String regularExpression) {
		Pattern pattern = Pattern.compile(regularExpression);
		Matcher matcher = pattern.matcher(address);
		if (matcher.find()) {
			return true;
		}
		return false;
	}

	private Boolean isHalfAlphNum(String value) {
		if (StringUtils.isEmpty(value))
			return true;
		if (value.length() == 0)
			return true;
		int len = value.length();
		byte[] bytes = value.getBytes();
		if (len != bytes.length)
			return false;
		return true;
	}

	private boolean checkXSS(String value) {
		boolean result = false;
		String[] splitValue = value.split("");
		String aText = ".*[<>&\"'].*";
		for (String chr : splitValue) {
			if (chr.matches(aText)) {
				result = true;
				break;
			}
		}
		return result;
	}

	//2017/06/28 変更分（候補で開業だけでも処理が通る問題）
	public boolean checkrn(String value) {
	    boolean result = false;
		//String str = "";

	    if(Pattern.compile("").matcher(value).matches()) {
	        result = true;
	    }
	    return result;
	}
	//ここまで

	/**
	 * イベントIDの有無で登録・更新処理を分ける
	 * イベントIDが存在しない⇒登録処理
	 * イベントIDが存在する⇒更新処理
	 */
	@Override
	public BaseBean process(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "process()");

		// エラーがある場合は処理を戻す
		if (obj.getError() != null && obj.getError().size() > 0) {
			return obj;
		}

		Wkmnag01Bean bean = (Wkmnag01Bean) obj;

		// イベントキー
		String eventKey = null;
		// イベントID
		String eventID = null;

		// イベントＩＤを保持していない場合
		if (StringUtils.isEmpty(bean.getCryptedEventId()) == false) {
			// イベントIDを複合化
			eventID = CryptoUtils.decrypto(bean.getCryptedEventId());
		}

		if (StringUtils.isEmpty(eventID)) {

			// ランダム文字列生成
			UUID u1 = UUID.randomUUID(); // 32文字＋ハイフン４つ
			UUID u2 = UUID.randomUUID(); // 32文字＋ハイフン４つ
			eventKey = (u1.toString() + u2.toString()).replaceAll("-", "");

			// イベントキーを暗号化
			eventKey = CryptoUtils.encrypto(eventKey);

			// T_EVENTテーブル INSERT呼び出し
			int regCnt = 0;
			ArrayList<String> bindValuesInsert = new ArrayList<String>();
			// VALUE句
			bindValuesInsert.add(eventKey);
			bindValuesInsert.add(bean.getEventName());
			bindValuesInsert.add(bean.getApplDeadline());
			bindValuesInsert.add(bean.getEventMemo());
			bindValuesInsert.add(bean.getEventMngrMailAddr());
			bindValuesInsert.add(bean.getEventPassword());
			bindValuesInsert.add("0");

			// SQL実行
			Wkmnag01Df03Dao ins = new Wkmnag01Df03Dao();
			regCnt = ins.update(con, bindValuesInsert);

			// エラー判定
			if (regCnt == 0) {
				throw new Waku2RuntimeException(ImmutableValues.format("MSGFTL0005", new String[] { "登録" }));
			}

			// 採番されたEVENT_IDを取得
			eventID = ins.getIdentity(con);
			bean.setCryptedEventId(CryptoUtils.encrypto(eventID));

			// 候補取得
			String date = bean.getEventDateText();
			String[] day = date.split("\r\n");

			// １行こどに登録
			for (int i = 0; i < day.length; i++) {
				// T_EVENT_DATEテーブル INSERT呼び出し
				bindValuesInsert = new ArrayList<String>();
				// VALUE句
				bindValuesInsert.add(eventID);
				bindValuesInsert.add(day[i]);
				bindValuesInsert.add("" + i);
				bindValuesInsert.add("0");

				// SQL実行
				Wkmnag01Df04Dao ins2 = new Wkmnag01Df04Dao();
				regCnt = ins2.update(con, bindValuesInsert);

				// エラー判定
				if (regCnt == 0) {
					throw new Waku2RuntimeException(ImmutableValues.format("MSGFTL0005", new String[] { "登録" }));
				}
			}
		} else {
			// T_EVENTテーブル UPDATE呼び出し
			int updCnt = 0;
			int regCnt = 0;
			ArrayList<String> bindValuesUpdate = new ArrayList<String>();
			// SET句
			bindValuesUpdate.add(bean.getEventName());
			bindValuesUpdate.add(bean.getEventMemo());
			bindValuesUpdate.add(bean.getApplDeadline());
			bindValuesUpdate.add(bean.getEventMngrMailAddr());
			bindValuesUpdate.add(bean.getEventPassword());
			// WHERE句
			bindValuesUpdate.add(eventID);

			// SQL実行
			Wkmnag01Df05Dao upd = new Wkmnag01Df05Dao();
			updCnt = upd.update(con, bindValuesUpdate);

			// エラー判定
			if (updCnt == 0) {
				throw new Waku2RuntimeException(ImmutableValues.format("MSGFTL0005", new String[] { "更新" }));
			}

			// T_EVENT_DATEテーブル UPDATE呼び出し（削除フラグ＝１に更新）
			bindValuesUpdate = new ArrayList<String>();
			// WHERE句
			bindValuesUpdate.add(eventID);

			// SQL実行
			Wkmnag01Df07Dao del = new Wkmnag01Df07Dao();
			updCnt = del.update(con, bindValuesUpdate);

			// 候補取得
			String date = bean.getEventDateText();
			String[] day = date.split("\r\n");

			// T_EVENT_DATEテーブル UPDATE/INSERT呼び出し（最新化）
			int cnt = 0;
			for (; cnt < day.length; cnt++) {
				// T_EVENT_DATEテーブル UPDATE呼び出し
				bindValuesUpdate = new ArrayList<String>();
				// SET句
				bindValuesUpdate.add("" + cnt);
				// WHERE句
				bindValuesUpdate.add(eventID);
				bindValuesUpdate.add(day[cnt]);

				// SQL実行
				Wkmnag01Df06Dao upd2 = new Wkmnag01Df06Dao();
				updCnt = upd2.update(con, bindValuesUpdate);

				// 更新結果確認
				if (updCnt == 0) {
					// 更新対象がなかった場合
					// T_EVENT_DATEテーブル INSERT呼び出し
					ArrayList<String> bindValuesInsert = new ArrayList<String>();
					// VALUE句
					bindValuesInsert.add(eventID);
					bindValuesInsert.add(day[cnt]);
					bindValuesInsert.add("" + cnt);
					bindValuesInsert.add("0");

					// SQL実行
					Wkmnag01Df04Dao ins2 = new Wkmnag01Df04Dao();
					regCnt = ins2.update(con, bindValuesInsert);

					// エラー判定
					if (regCnt == 0) {
						throw new Waku2RuntimeException(ImmutableValues.format("MSGFTL0005", new String[] { "登録" }));
					}
				}
			}

		}

		// イベントキーを保持していない場合はイベントキーを取得
		if (StringUtils.isEmpty(eventKey)) {
			ArrayList<String> bindValuesSelect = new ArrayList<String>();
			bindValuesSelect.add(eventID);
			List<Map<String, String>> resultDf01 = new Wkmnag01Df01Dao().select(con, bindValuesSelect);
			eventKey = (resultDf01.get(0)).get(Wkmnag01Df01Dao.EVENT_KEY);
		}

		// 管理者メール
		String[] mlAddr = StringUtils.isEmpty(bean.getEventMngrMailAddr()) ? new String[0]
				: bean.getEventMngrMailAddr().split(",");
		String pass = bean.getEventPassword();
		for (String mail : mlAddr) {
			if (StringUtils.isEmpty(mail) == false) {
				MailUtils.sendMailToManager(mail, eventKey, "", pass);
			}
		}

		bean.setRedirectURL("/waku2/manage/WKMNAG02.html?cei=" + bean.getCryptedEventId());

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "process()");

		return bean;
	}

}
