package com.humansystem.waku2.action;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.bean.Wklgin01Bean;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.dao.Wklgin01Df01Dao;
import com.humansystem.waku2.exception.Waku2RuntimeException;
import com.humansystem.waku2.util.StringUtils;

/**
 * WKLGIN01/管理者ログインのActionクラス
 * <p>
 * WKLGIN01_EV_01
 * <p/>
 *
 * ログイン画面を表示する
 *
 * create-date 19/05/2017.
 *
 * @author morita_k
 */
public class Wklgin01Ev01Action extends BaseAction {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wklgin01Ev01Action.class);

	/**
	 * リクエスト情報からekを取得する
	 */
	@Override
	public BaseBean populate(HttpServletRequest req, HttpServletResponse resp, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "populate()");

		// 画面Beanのインスタンスを作成
		Wklgin01Bean bean = new Wklgin01Bean();

		// パラメータ：ekからイベントキーを取得する。
		if (StringUtils.isEmpty(req.getParameter("ek")) == false) {
			bean.setEventKey(req.getParameter("ek"));
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "populate()");

		return bean;
	}

	/**
	 * 入力チェックなし
	 */
	@Override
	public void validate(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		// 入力チェックなし
	}

	/**
	 * イベントキーからイベント情報を抽出し、画面項目に設定する
	 */
	@Override
	public BaseBean process(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "process()");

		Wklgin01Bean bean = (Wklgin01Bean) obj;

		List<String> select = new ArrayList<String>();

		select.add(bean.getEventPassword());
		select.add(bean.getEventKey());
		select.add(bean.getEventPassword());// イベントキー
		select.add(bean.getEventKey());

		// イベントキーからイベント情報を取得する。
		List<Map<String, String>> resultDf01 = new Wklgin01Df01Dao().select(con, select);

		// イベント情報が1件取得できなかった場合はエラー
		if (resultDf01.size() != 1) {
			throw new Waku2RuntimeException(ImmutableValues.MSGFTL0008);
		} else {
			bean.setEventName(resultDf01.get(0).get(Wklgin01Df01Dao.EVENT_NAME));
			bean.setCryptedEventId(resultDf01.get(0).get(Wklgin01Df01Dao.EVENT_ID));
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "process()");

		return bean;
	}

}
