package com.humansystem.waku2.action;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.bean.Wkinpt01Bean;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.util.StringUtils;

/**
 * WKINPT01/わくわく調整表のBeanクラス
 *
 * create-date 01/05/2017.
 *
 * ○△×入力画面に遷移する
 *
 * @author 作成者:ishii_y 修正者:morita_k
 */

public class Wkinpt01Ev03Action extends BaseAction{
	private static Logger log = LoggerFactory.getLogger(Wkinpt01Ev03Action.class);

	/**
	 * リクエスト情報から、画面項目の情報とekを取得し、Beanに設定する
	 */
	@Override
	public BaseBean populate(HttpServletRequest req, HttpServletResponse resp, Connection con) {
		/** Logger */
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "populate()");

		Wkinpt01Bean bean = (Wkinpt01Bean) req.getAttribute("__bean__");
		//イベントキーを取得
		if (StringUtils.isEmpty(req.getParameter("ek")) == false) {
			bean.setEventKey(req.getParameter("ek"));
		}


		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "populate()");
		return bean;
	}

	/**
	 * 入力チェックなし
	 */
	@Override
	public void validate(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		//リンクの値をチェック？
	}

	/**
	 * ○△×入力画面に遷移する処理(リンク押下時)
	 */
	@Override
	public BaseBean process(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "process()");

		Wkinpt01Bean bean = (Wkinpt01Bean) obj;


		bean.setRedirectURL("/waku2/manage/WKINPT02.html?ek=" + bean.getEventKey());

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "process()");

		return bean;
	}
}