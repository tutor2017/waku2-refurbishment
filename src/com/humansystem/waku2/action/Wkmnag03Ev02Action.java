package com.humansystem.waku2.action;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.bean.Wkmnag03Bean;
import com.humansystem.waku2.common.ImmutableValues;

/**
 * WKINPT03/完了のBeanクラス
 *
 * create-date 21/05/2017.
 *
 * 説明：ログイン画面に遷移する
 *
 *@author Morita_k
 *check :Mizobe_n
 */


public class Wkmnag03Ev02Action extends BaseAction {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag03Ev02Action.class);

	/**
	 * 画面のリクエスト情報を取得し、Beanに設定する
	 */
	@Override
	public BaseBean populate(HttpServletRequest req, HttpServletResponse resp, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "populate()");

		Wkmnag03Bean bean = (Wkmnag03Bean) req.getAttribute("__bean__");

		if (bean == null) {
			bean = new Wkmnag03Bean();
		}

		bean.setEventKey(req.getParameter("ek"));

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "populate()");

		return bean;
	}

	/**
	 * 入力チェックなし
	 */
	@Override
	public void validate(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		//未使用
	}

	/**
	 * ログイン画面に遷移する
	 */
	@Override
	public BaseBean process(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "process()");

		Wkmnag03Bean bean = (Wkmnag03Bean) obj;

		bean.setRedirectURL("/waku2/login/WKLGIN01.html?ek=" + bean.getEventKey());

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "process()");

		return bean;
	}

}
