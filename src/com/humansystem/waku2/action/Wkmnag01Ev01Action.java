package com.humansystem.waku2.action;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.bean.Wkmnag01Bean;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.dao.Wkmnag01Df01Dao;
import com.humansystem.waku2.dao.Wkmnag01Df02Dao;
import com.humansystem.waku2.exception.Waku2RuntimeException;
import com.humansystem.waku2.util.CryptoUtils;
import com.humansystem.waku2.util.StringUtils;

/**
 * WKMNAG01/わくわく調整表のActionクラス
 * <p>WKMNAG01_EV_01<p/>
 *
 * 「わくわく調整表作成」画面を表示する
 *
 * create-date 01/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
public class Wkmnag01Ev01Action extends BaseAction {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag01Ev01Action.class);

	/**
	 * パラメータからceiを取得し、設定する
	 */
	@Override
	public BaseBean populate(HttpServletRequest req, HttpServletResponse resp, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "populate()");

		// 画面Beanのインスタンスを作成
		Wkmnag01Bean bean = new Wkmnag01Bean();

		// パラメータ：ceiから暗号化イベントIDを取得する。
		if (StringUtils.isEmpty(req.getParameter("cei")) == false) {
			bean.setCryptedEventId(req.getParameter("cei"));
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "populate()");

		return bean;
	}

	/**
	 * 入力チェックなし
	 */
	@Override
	public void validate(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		// 入力チェックなし
	}

	/**
	 * DBからイベントの情報を取得し、画面項目に設定する
	 */
	@Override
	public BaseBean process(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "process()");

		Wkmnag01Bean bean = (Wkmnag01Bean) obj;

		// 暗号化イベントIDを保持している場合
		if (StringUtils.isEmpty(bean.getCryptedEventId()) == false) {

			List<String> bindValuesDf01 = new ArrayList<String>();
			bindValuesDf01.add(CryptoUtils.decrypto(bean.getCryptedEventId())); // 複合化したイベントID

			// 暗号化イベントIDからイベント情報を取得する。
			List<Map<String, String>> resultDf01 = new Wkmnag01Df01Dao().select(con, bindValuesDf01);

			// イベント情報が1件取得できなかった場合はエラー
			if(resultDf01.size() != 1){
				throw new Waku2RuntimeException(ImmutableValues.MSGFTL0008);
			}

			// イベント情報を画面項目にセットする。
			for (Map<String, String> m : resultDf01) {
				// イベント名
				bean.setEventName(m.get(Wkmnag01Df01Dao.EVENT_NAME));
				// メモ
				bean.setEventMemo(m.get(Wkmnag01Df01Dao.EVENT_MEMO));
				// 締切日
				bean.setApplDeadline(m.get(Wkmnag01Df01Dao.APPL_DEADLINE));
				// 管理者メールアドレス
				bean.setEventMngrMailAddr(m.get(Wkmnag01Df01Dao.EVENT_MNGR_MAIL_ADDR));
			}

			List<String> bindValuesDf02 = new ArrayList<String>();
			bindValuesDf02.add(CryptoUtils.decrypto(bean.getCryptedEventId())); // 複合化したイベントID
			// イベント日程を取得する。
			List<Map<String, String>> resultDf02 = new Wkmnag01Df02Dao().select(con, bindValuesDf02);

			// イベント日程が取得できなかった場合はエラー
			if (resultDf02 == null || resultDf02.size() == 0) {
				throw new Waku2RuntimeException(ImmutableValues.MSGFTL0008);
			}

			// イベント日程を画面項目にセットする。
			StringBuilder eventDateText = new StringBuilder();
			for (Map<String, String> m : resultDf02) {

				if (eventDateText.length() != 0) {
					// 改行コードを追加
					eventDateText.append("\n");
				}
				eventDateText.append(m.get(Wkmnag01Df02Dao.EVENT_DATE_TEXT));

			}

			// 候補
			bean.setEventDateText(eventDateText.toString());

		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "process()");

		return bean;
	}

}
