﻿package com.humansystem.waku2.action;
import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.bean.Wkinpt01Bean;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.util.StringUtils;

/**
 * WKINPT01/わくわく調整表のBeanクラス
 *
 * create-date 01/05/2017.
 *
 * 説明：○△×の入力画面に遷移する
 *
 * @author 作成者:ishii_y 修正者:morita_k
 */

public class Wkinpt01Ev02Action extends BaseAction{
	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkinpt01Ev02Action.class);

	/**
	 * リクエスト情報から、画面項目の情報を取得し、Beanに設定する
	 */
	@Override
	public BaseBean populate(HttpServletRequest req, HttpServletResponse resp, Connection con) {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "populate()");

		Wkinpt01Bean bean = (Wkinpt01Bean) req.getAttribute("__bean__");

		if (bean == null) {
			bean = new Wkinpt01Bean();
		}

		// イベントキーを取得
		bean.setEventKey(req.getParameter("eventKey"));
		// メールアドレス取得
		bean.setEventApplAddr(req.getParameter("eventApplAddr"));

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "populate()");
		return bean;
	}

	/**
	 * 入力チェックなし
	 */
	@Override
	public void validate(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {


	}

	/**
	 * ○△×入力画面に遷移する処理(ボタンからの遷移)
	 */
	@Override
	public BaseBean process(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "process()");

		Wkinpt01Bean bean = (Wkinpt01Bean) obj;

		if (StringUtils.isEmpty(bean.getEventApplAddr()) == false) {
			bean.setRedirectURL("/waku2/input/WKINPT02.html?ek=" + bean.getEventKey() + "&m=" + bean.getEventApplAddr());
		} else {
			bean.setRedirectURL("/waku2/input/WKINPT02.html?ek=" + bean.getEventKey());
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "process()");

		return bean;
	}
}