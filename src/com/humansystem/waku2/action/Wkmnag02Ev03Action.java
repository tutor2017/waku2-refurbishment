package com.humansystem.waku2.action;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.bean.Wkmnag02Bean;
import com.humansystem.waku2.common.ImmutableValues;


/**
 * WKMNAG02/参加者を募る画面のActionクラス
 * <p>
 * WKMNAG02_EV_03
 * <p/>
 *
 * URLのコピーの処理を行う
 *
 * create-date 21/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */

public class Wkmnag02Ev03Action extends BaseAction{

	private static Logger log = LoggerFactory.getLogger(Wkmnag02Ev03Action.class);

	/**
	 * リクエスト情報から、画面項目の情報を取得し、Beanに設定する
	 */
	@Override
	public BaseBean populate(HttpServletRequest req, HttpServletResponse resp, Connection con) {

		log.info(ImmutableValues.MSGLOG0003, "populate()");

		Wkmnag02Bean bean = (Wkmnag02Bean) req.getAttribute("__bean__");

		if (bean == null) {
			bean = new Wkmnag02Bean();
		}

		String mem = req.getParameter("memberContact");

		if (mem == null) {
			mem = "0";
		}

		bean.setMemberContact(mem);

		bean.setChouseiUrl(req.getParameter("chouseiUrl"));
		//bean.setMemberContact(req.getParameter("memberContact"));
		//bean.setMemberContact(mem);
		bean.setMailText(req.getParameter("mailText"));
		bean.setEventMngrMailAddr(req.getParameter("eventMngrMailAddr"));
		bean.setParticipantMailaddress(req.getParameter("participantMailaddress"));
		bean.setAllMembersSendMail(req.getParameter("setAllMembersSendMail"));
		bean.setCryptedEventId(req.getParameter("cryptedEventId"));
		bean.setContactChangeFlag(req.getParameter("contactChangeFlag"));
		bean.setEventKey(req.getParameter("eventKey"));
		bean.setApplDeadline(req.getParameter("applDeadline"));

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "populate()");
		return bean;
	}
	/**
	 * 入力チェックなし
	 */
	@Override
	public void validate(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		//未使用

	}

	/**
	 * URLのコピー処理
	 */
	@Override
	public BaseBean process(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "process()");

		Wkmnag02Bean bean = (Wkmnag02Bean) obj;

		Toolkit kit = Toolkit.getDefaultToolkit();
		Clipboard clip = kit.getSystemClipboard();
		StringSelection ss = new StringSelection(bean.getChouseiUrl());
		clip.setContents(ss, ss);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "process()");

		return bean;
	}

}
