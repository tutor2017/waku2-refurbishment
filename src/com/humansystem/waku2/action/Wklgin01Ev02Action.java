package com.humansystem.waku2.action;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.bean.Wklgin01Bean;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.dao.Wklgin01Df01Dao;
import com.humansystem.waku2.util.CryptoUtils;
import com.humansystem.waku2.util.StringUtils;

/**
 * WKLGIN01/管理者ログインのActionクラス
 * <p>
 * WKLGIN01_EV_02
 * <p/>
 *
 * 説明：ログインをする
 *
 * create-date 119/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
public class Wklgin01Ev02Action extends BaseAction {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wklgin01Ev02Action.class);

	/**
	 * リクエスト情報から、画面項目の情報を取得し、Beanに設定する
	 */
	@Override
	public BaseBean populate(HttpServletRequest req, HttpServletResponse resp, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "populate()");

		Wklgin01Bean bean = (Wklgin01Bean) req.getAttribute("__bean__");

		if (bean == null) {
			bean = new Wklgin01Bean();
		}

		bean.setEventKey(req.getParameter("eventKey"));
		bean.setEventName(req.getParameter("eventName"));
		bean.setEventPassword(req.getParameter("eventPassword"));

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "populate()");

		return bean;
	}

	/**
	 * 入力チェックを行う
	 */
	@Override
	public void validate(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "validate()");

		// イベントパスワード
		String target = ((Wklgin01Bean) obj).getEventPassword();
		String name = "イベントパスワード";
		// 必須
		if (StringUtils.isEmpty(target)) {
			obj.addError(ImmutableValues.format("MSGERR0001", new String[] { name }));
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "validate()");

	}

	/**
	 * ログインを行う
	 */
	@Override
	public BaseBean process(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "process()");

		// エラーがある場合は処理を戻す
		if (obj.getError() != null && obj.getError().size() > 0) {
			return obj;
		}

		Wklgin01Bean bean = (Wklgin01Bean) obj;

		List<String> select = new ArrayList<String>();
		select.add(bean.getEventPassword());
		select.add(bean.getEventKey()); // イベントキー
		select.add(bean.getEventPassword());// イベントパスワード
		select.add(bean.getEventKey());

		// イベントキーとイベントパスワードからイベント情報を取得する。
		List<Map<String, String>> resultDf01 = new Wklgin01Df01Dao().select(con, select);

		// イベント情報が1件取得できなかった場合はエラー
		if (resultDf01.size() != 1) {
			bean.addError(ImmutableValues.format("MSGERR0020", new String[] { "イベントキー", "イベントパスワード" }));
			// わくわく調整表作成画面に引数イベントIDを付与
		} else {
			bean.setRedirectURL("/waku2/manage/WKMNAG01.html?cei="
					+ CryptoUtils.encrypto(resultDf01.get(0).get(Wklgin01Df01Dao.EVENT_ID)));
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "process()");

		return bean;
	}

}
