package com.humansystem.waku2.action;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.bean.Wkinpt02Bean;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.dao.Wkinpt02Df04Dao;
import com.humansystem.waku2.dao.Wkinpt02Df05Dao;
import com.humansystem.waku2.dao.Wkinpt02Df06Dao;
import com.humansystem.waku2.dao.Wkinpt02Df07Dao;
import com.humansystem.waku2.exception.Waku2RuntimeException;
import com.humansystem.waku2.util.CryptoUtils;
import com.humansystem.waku2.util.StringUtils;

/**
 * WKMNAG02/○△✕入力のActionクラス
 * <p>
 * WKINPT02_EV_02
 * <p/>
 *
 * 画面の入力情報の登録を行う
 *
 * create-date 19/05/2017.
 *
 * @author mizobe_n (revision:kasai_r) @
 */

public class Wkinpt02Ev02Action extends BaseAction {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkinpt02Ev02Action.class);

	/**
	 * リクエスト情報から、画面項目の情報を取得し、Beanに設定する
	 */
	@Override
	public BaseBean populate(HttpServletRequest req, HttpServletResponse resp, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "populate()");

		Wkinpt02Bean bean = (Wkinpt02Bean) req.getAttribute("__bean__");

		if (bean == null) {
			bean = new Wkinpt02Bean();
		}

		int eventListSize = Integer.valueOf(req.getParameter("eventListSize"));

		// 上記beanに一括でデータを返すためのlist
		List<Wkinpt02Bean> eventDateList = new ArrayList<Wkinpt02Bean>();

		// 複数件情報が入力される項目用の繰り返し
		for (int i = 0; i < eventListSize; i++) {
			Wkinpt02Bean listBean = new Wkinpt02Bean();
			listBean.setEventDateText(req.getParameterValues("eventDateText")[i]);
			listBean.setApplType(req.getParameterValues("applType")[i]);
			listBean.setCryptedEventDateId(req.getParameterValues("cryptedEventDateId")[i]);
			eventDateList.add(listBean);
		}

		bean.setEventKey(req.getParameter("eventKey"));
		bean.setApplName(req.getParameter("applName"));
		bean.setApplComment(req.getParameter("applComment"));
		bean.setCryptedEventId(req.getParameter("cryptedEventId"));
		bean.setApplMailAddr(req.getParameter("applMailAddr"));
		bean.setCryptedEventAttendId(req.getParameter("cryptedEventAttendId"));
		bean.setEventDateList(eventDateList);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "populate()");

		return bean;
	}

	/**
	 * 入力チェックを行う
	 */
	@Override
	public void validate(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "validate()");

		// 入力チェックあり
		// 表示名
		String target = ((Wkinpt02Bean) obj).getApplName();
		String name = "表示名";

		if (StringUtils.isEmpty(target)) {
			obj.addError(ImmutableValues.format("MSGERR0001", new String[] { name }));
		} else {
			if (target.length() > 50) {
				obj.addError(ImmutableValues.format("MSGERR0002", new String[] { name, "50" }));
			}
			if(checkXSS(target)) {
				((Wkinpt02Bean) obj).setApplName("");
				obj.addError(ImmutableValues.format("MSGERR0017", new String[] { name }));
			}
		}

		// コメント文
		target = ((Wkinpt02Bean) obj).getApplComment();
		name = "コメント";

		if (target.length() > 500) {
			obj.addError(ImmutableValues.format("MSGERR0002", new String[] { name, "500" }));
		}
		if(!(target.equals("")) && (checkXSS(target) || checkSpaces(target))) {
			((Wkinpt02Bean) obj).setApplComment("");
			obj.addError(ImmutableValues.format("MSGERR0017", new String[] { name }));
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "validate()");
	}

	private boolean checkSpaces(String value) {
		String aText = "^ *$";
		if(value.matches(aText)) {
			return true;
		} else {
			return false;
		}
	}

	private boolean checkXSS(String value) {
		boolean result = false;
		String[] splitValue = value.split("");
		String aText = ".*[<>&\"'].*";
		for (String chr : splitValue) {
			if (chr.matches(aText)) {
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * ○△×の入力情報の登録を行う
	 */
	@Override
	public BaseBean process(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "process()");

		// エラーがある場合
		if (obj.getError() != null && obj.getError().size() > 0) {
			return obj;
		}
		int regCnt = 0;
		String name = null;
		Wkinpt02Bean bean = (Wkinpt02Bean) obj;
		String eventAttendId = null;

		if (StringUtils.isEmpty(bean.getCryptedEventAttendId()) == false) {
			name = "更新";
			ArrayList<String> bindValuesInsert = new ArrayList<String>();
			// VALUE句
			bindValuesInsert.add(bean.getApplName());
			bindValuesInsert.add(bean.getApplComment());
			bindValuesInsert.add(decryptoEventAttendId(bean));

			// SQL実行
			Wkinpt02Df04Dao ins = new Wkinpt02Df04Dao();
			regCnt = ins.update(con, bindValuesInsert);

			eventAttendId = decryptoEventAttendId(bean);

		} else {
			name = "登録";
			ArrayList<String> bindValuesInsert = new ArrayList<String>();

			// VALUE句
			bindValuesInsert.add(decryptoEventId(bean));
			bindValuesInsert.add(bean.getApplName());
			bindValuesInsert.add(bean.getApplComment());
			bindValuesInsert.add(CryptoUtils.decrypto(bean.getApplMailAddr()));

			// SQL実行
			Wkinpt02Df05Dao ins = new Wkinpt02Df05Dao();
			regCnt = ins.update(con, bindValuesInsert);

			// 採番されたEVENT_IDを取得
			eventAttendId = ins.getIdentity(con);

		}

		// エラー判定
		if (regCnt == 0) {
			throw new Waku2RuntimeException(ImmutableValues.format("MSGFTL0005", new String[] { name }));
		}

		// 日程の数繰り返して登録する
		for (Wkinpt02Bean loopBean : bean.getEventDateList()) {
				name = "更新";
				ArrayList<String> bindValuesInsert = new ArrayList<String>();
				// VALUE句
				bindValuesInsert.add(loopBean.getApplType());
				bindValuesInsert.add(decryptoEventDateId(loopBean));
			bindValuesInsert.add(eventAttendId);

				// SQL実行
				Wkinpt02Df06Dao ins = new Wkinpt02Df06Dao();
				regCnt = ins.update(con, bindValuesInsert);
			if (regCnt == 0) {
				name = "登録";
				ArrayList<String> bindValuesInsert2 = new ArrayList<String>();
				// VALUE句
				bindValuesInsert2.add(decryptoEventDateId(loopBean));
				bindValuesInsert2.add(eventAttendId);
				bindValuesInsert2.add(loopBean.getApplType());

					// SQL実行
				Wkinpt02Df07Dao ins2 = new Wkinpt02Df07Dao();
				regCnt = ins2.update(con, bindValuesInsert2);
			}
		}

		// エラー判定
		if (regCnt == 0) {
			throw new Waku2RuntimeException(ImmutableValues.format("MSGFTL0005", new String[] { name }));
		}

		// 遷移先URL（わくわく調整表
		if (bean.getApplMailAddr().equals("")) {
			bean.setRedirectURL("/waku2/input/WKINPT01.html?ek=" + bean.getEventKey());
		} else {
			bean.setRedirectURL("/waku2/input/WKINPT01.html?ek=" + bean.getEventKey() + "&m=" + bean.getApplMailAddr());
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "process()");

		return bean;

	}

	String decryptoEventAttendId(Wkinpt02Bean bean) {
		String eventAttendId = null;
		if (StringUtils.isEmpty(bean.getCryptedEventAttendId()) == false) {
			eventAttendId = CryptoUtils.decrypto(bean.getCryptedEventAttendId());
		}
		return eventAttendId;
	}

	String decryptoEventId(Wkinpt02Bean bean) {
		String eventId = null;
		if (StringUtils.isEmpty(bean.getCryptedEventId()) == false) {
			eventId = CryptoUtils.decrypto(bean.getCryptedEventId());
		}
		return eventId;
	}

	String decryptoEventDateId(Wkinpt02Bean bean) {
		String eventDateId = null;
		if (StringUtils.isEmpty(bean.getCryptedEventDateId()) == false) {
			eventDateId = CryptoUtils.decrypto(bean.getCryptedEventDateId());
		}
		return eventDateId;
	}

}
