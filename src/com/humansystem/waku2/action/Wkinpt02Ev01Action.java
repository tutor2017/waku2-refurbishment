package com.humansystem.waku2.action;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.bean.Wkinpt02Bean;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.dao.Wkinpt02Df01Dao;
import com.humansystem.waku2.dao.Wkinpt02Df02Dao;
import com.humansystem.waku2.dao.Wkinpt02Df03Dao;
import com.humansystem.waku2.exception.Waku2RuntimeException;
import com.humansystem.waku2.util.CryptoUtils;
import com.humansystem.waku2.util.StringUtils;

/**
 * WKMNAG01/○△✕入力のActionクラス
 * <p>WKINPT02_EV_01<p/>
 *
 * ○△×入力画面を表示する
 *
 * create-date 19/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */

public class Wkinpt02Ev01Action  extends BaseAction{

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkinpt02Ev01Action.class);

	/**
	 * リクエストパラメータから情報を取得する
	 */
	@Override
	public BaseBean populate(HttpServletRequest req, HttpServletResponse resp, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "populate()");

		// 画面Beanのインスタンスを作成
		Wkinpt02Bean bean = new Wkinpt02Bean();

		// パラメータ：イベントキーを取得する。
		if(StringUtils.isEmpty(req.getParameter("ek")) == false){
			bean.setEventKey(req.getParameter("ek"));
		}else {
			throw new Waku2RuntimeException(ImmutableValues.MSGFTL0008);
		}

		// パラメータ：ceaiから暗号化出欠イベントIDを取得する。
		if(StringUtils.isEmpty(req.getParameter("ceai")) == false){
			bean.setCryptedEventAttendId(req.getParameter("ceai"));
		}

		// パラメータ：mからメールドレスを取得する。
		if(StringUtils.isEmpty(req.getParameter("m")) == false){
			bean.setApplMailAddr(req.getParameter("m"));
		}

		// 終了ログ
				log.info(ImmutableValues.MSGLOG0004, "populate()");

		return bean;
	}

	/**
	 * 入力チェックなし
	 */
	@Override
	public void validate(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		// 入力チェックなし
	}

	/**
	 * 画面の表示項目の取得し、設定する
	 */
	@Override
	public BaseBean process(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "process()");

		Wkinpt02Bean bean = (Wkinpt02Bean) obj;
		String eventId = null;

		// URLパラメータから取得したイベントキーを元にイベントID取得
		List<String> Wkinpt02df01 = new ArrayList<String>();
		Wkinpt02df01.add(bean.getEventKey()); // イベントキー

		List<Map<String,String>> resultDf01 = new Wkinpt02Df01Dao().select(con,Wkinpt02df01);

		if (resultDf01.size() != 1) {
			throw new Waku2RuntimeException(ImmutableValues.MSGFTL0008);
		} else {
			eventId = resultDf01.get(0).get(Wkinpt02Df01Dao.EVENT_ID);
			bean.setCryptedEventId(CryptoUtils.encrypto(eventId));
		}

		//候補リスト
		List<Wkinpt02Bean> eventDateList = new ArrayList<Wkinpt02Bean>();

		// 画面表示情報の取得：暗号化イベント出欠IDを保持している場合
		if (StringUtils.isEmpty(bean.getCryptedEventAttendId()) == false) {

			String eventAttendId = CryptoUtils.decrypto(bean.getCryptedEventAttendId());

			List<String> Wkinpt02df02 = new ArrayList<String>();
			Wkinpt02df02.add(eventId); // イベントID
			Wkinpt02df02.add(eventAttendId);

			List<Map<String,String>>  resultDf02 = new Wkinpt02Df02Dao().select(con,Wkinpt02df02);

			// イベント日程が取得できなかった場合はエラー
			if (resultDf02 == null || resultDf02.size() == 0) {
				throw new Waku2RuntimeException(ImmutableValues.MSGFTL0008);
			}

			// イベント入力情報を画面項目にセットする。
			for(Map<String, String> m : resultDf02){
				if(eventDateList.size() == 0) {
					bean.setApplName(m.get(Wkinpt02Df02Dao.APPL_NAME));
					bean.setApplComment(m.get(Wkinpt02Df02Dao.APPL_COMMENT));
				}
				Wkinpt02Bean wkinpt02bean = new Wkinpt02Bean();
				wkinpt02bean.setCryptedEventDateId(CryptoUtils.encrypto(m.get(Wkinpt02Df02Dao.EVENT_DATE_ID)));
				wkinpt02bean.setEventDateText(m.get(Wkinpt02Df02Dao.EVENT_DATE_TEXT));
				wkinpt02bean.setApplType(m.get(Wkinpt02Df02Dao.APPL_TYPE));
				eventDateList.add(wkinpt02bean);
			}

		} else {

			List<String> Wkinpt02df03 = new ArrayList<String>();
			Wkinpt02df03.add(eventId); // イベントID

			List<Map<String,String>> resultDf03 = new Wkinpt02Df03Dao().select(con,Wkinpt02df03);

			// イベント日程が取得できなかった場合はエラー
			if (resultDf03 == null || resultDf03.size() == 0) {
				throw new Waku2RuntimeException(ImmutableValues.MSGFTL0008);
			}

			// イベント入力情報を画面項目にセットする。
			for(Map<String, String> m : resultDf03){
				Wkinpt02Bean wkinpt02bean = new Wkinpt02Bean();
				wkinpt02bean.setCryptedEventDateId(CryptoUtils.encrypto(m.get(Wkinpt02Df03Dao.EVENT_DATE_ID)));
				wkinpt02bean.setEventDateText(m.get(Wkinpt02Df03Dao.EVENT_DATE_TEXT));
				eventDateList.add(wkinpt02bean);
			}
		}

		bean.setEventDateList(eventDateList);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "process()");

		return bean;

}

}
