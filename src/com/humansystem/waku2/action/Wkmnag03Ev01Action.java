package com.humansystem.waku2.action;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.bean.Wkmnag03Bean;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.exception.Waku2RuntimeException;
import com.humansystem.waku2.util.StringUtils;

/**
 * WKMNAG03/完了のActionクラス
 * <p>
 * WKMNAG03_EV_01
 * <p/>
 *
 * 説明：完了画面を表示する
 *
 * create-date 21/05/2017.
 *
 * @author Morita_k
 * check :Mizobe_n

 */

public class Wkmnag03Ev01Action extends BaseAction {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag03Ev01Action.class);

	/**
	 * リクエスト情報からekを取得する
	 */
	@Override
	public BaseBean populate(HttpServletRequest req, HttpServletResponse resp, Connection con) {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "populate()");

		// 画面Beanのインスタンスを作成
		Wkmnag03Bean bean = new Wkmnag03Bean();

		// パラメータ：ekからイベントキーを取得する。
		if (StringUtils.isEmpty(req.getParameter("ek")) == false) {
			bean.setEventKey(req.getParameter("ek"));
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "populate()");

		return bean;
	}

	/**
	 * 入力チェックなし
	 */
	@Override
	public void validate(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		//入力チェックなし
	}

	/**
	 * 画面の表示を行う
	 */
	@Override
	public BaseBean process(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {

		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "process()");

		Wkmnag03Bean bean = (Wkmnag03Bean) obj;

		// イベントキーが取得できなかった場合はエラー
		if(StringUtils.isEmpty(bean.getEventKey()) == true){
			throw new Waku2RuntimeException(ImmutableValues.MSGFTL0008);
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "process()");

		return bean;
	}

}
