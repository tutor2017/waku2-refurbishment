package com.humansystem.waku2.action;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.humansystem.waku2.bean.BaseBean;

/**
 * Actionの親クラス
 *
 * create-date 27/04/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */
abstract public class BaseAction {

	/**
	 * 画面からPostもしくはGetされた情報をBeanとして格納する処理
	 */
	abstract public BaseBean populate(HttpServletRequest req, HttpServletResponse resp,Connection con);

	/**
	 * Beanに格納された情報に対して値のチェックを行う処理
	 */
	abstract public void validate(HttpServletRequest req, HttpServletResponse resp,BaseBean obj,Connection con);

	/**
	 * DBからの情報取得や登録処理などのビジネス処理
	 */
	abstract public BaseBean process(HttpServletRequest req, HttpServletResponse resp,BaseBean obj,Connection con);

}
