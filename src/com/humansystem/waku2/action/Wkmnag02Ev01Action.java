package com.humansystem.waku2.action;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.bean.Wkmnag02Bean;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.dao.Wkmnag02Df01Dao;
import com.humansystem.waku2.dao.Wkmnag02Df02Dao;
import com.humansystem.waku2.exception.Waku2RuntimeException;
import com.humansystem.waku2.util.CryptoUtils;
import com.humansystem.waku2.util.StringUtils;

/**
 * WKMNAG02/参加メンバーを募るのActionクラス
 * <p>
 * WKMNAG02_EV_01
 * <p/>
 *
 * 「参加メンバーを募る」画面を表示する
 *
 * create-date 22/05/2017.
 *
 */

public class Wkmnag02Ev01Action extends BaseAction {

	private static Logger log = LoggerFactory.getLogger(Wkmnag02Ev01Action.class);

	/**
	 * パラメータからceiを取得し、設定する
	 */
	@Override
	public BaseBean populate(HttpServletRequest req, HttpServletResponse resp, Connection con) {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "populate()");

		// 画面Beanのインスタンスを作成
		Wkmnag02Bean bean = new Wkmnag02Bean();

		// パラメータ：ceiから暗号化イベントIDを取得する。
		if(StringUtils.isEmpty(req.getParameter("cei")) == false){
			bean.setCryptedEventId(req.getParameter("cei"));
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "populate()");

		return bean;
	}

	/**
	 * 入力チェックなし
	 */
	@Override
	public void validate(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		//未使用
	}

	/**
	 * イベント情報を取得し、画面項目に設定する
	 */
	@Override
	public BaseBean process(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "process()");

		Wkmnag02Bean bean = (Wkmnag02Bean) obj;

		List<String> bindValuesDf02 = new ArrayList<String>();
		bindValuesDf02.add(CryptoUtils.decrypto(bean.getCryptedEventId())); // 複合化したイベントID

		// 暗号化イベントIDからイベント情報を取得する。
		List<Map<String, String>> resultDf01 = new Wkmnag02Df01Dao().select(con, bindValuesDf02);

		// イベント情報が1件取得できなかった場合はエラー
		if(resultDf01.size() != 1){
			throw new Waku2RuntimeException(ImmutableValues.MSGFTL0008);
		}

		// イベント情報を画面項目にセットする。
		for(Map<String, String> m : resultDf01){
			// イベントキー
			bean.setEventKey(m.get(Wkmnag02Df01Dao.EVENT_KEY));
			// わくわく調整表URL
			bean.setChouseiUrl(ImmutableValues.SITE_URL + "/waku2/input/WKINPT01.html?ek=" + bean.getEventKey());
			// 管理者メールアドレス
			bean.setEventMngrMailAddr(CryptoUtils.encrypto(m.get(Wkmnag02Df01Dao.EVENT_MNGR_MAIL_ADDR)));
			// 参加メンバーへの連絡を行う
			bean.setMemberContact(m.get(Wkmnag02Df01Dao.CNTCT_FLG));

			// イベントIDが格納されていない場合
			if (bean.getCryptedEventId().matches("")) {
				// 初期値：メモ
				bean.setMailText(m.get(Wkmnag02Df01Dao.EVENT_MEMO));
			} else {
			// メールテキスト
			bean.setMailText(m.get(Wkmnag02Df01Dao.CNTCT_MAIL_BODY));
			}
			// 参加者メールアドレス
			bean.setParticipantMailaddress(m.get(Wkmnag02Df01Dao.CNTCT_MAIL_ADDR));
			// 締切日
			bean.setApplDeadline(m.get(Wkmnag02Df01Dao.APPL_DEADLINE));

		}

		// イベントIDから申込者メアドを取得する
		List<Map<String, String>> resultDf02 = new Wkmnag02Df02Dao().select(con, bindValuesDf02);

		// イベント出欠が0件だった場合、連絡変更可能フラグを立てる
		if(resultDf02.size() == 0){

			bean.setContactChangeFlag("ON");

		}else{

			bean.setContactChangeFlag("OFF");

		}

		// 取得した申込者メアドを画面項目にセット
		StringBuilder participantMailaddress = new StringBuilder();

		for (Map<String, String> m : resultDf02) {
			if (participantMailaddress.length() != 0) {
				// <br>タグの追加
				participantMailaddress.append("<br>");
			}
			// 回答済みメールアドレス
			participantMailaddress.append(m.get(Wkmnag02Df02Dao.APPL_MAIL_ADDR));
		}

		String ansMails = new String(participantMailaddress);
		bean.setAnsweredMailaddress(ansMails);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "process()");

		return bean;

	}

}
