package com.humansystem.waku2.action;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.bean.Wkmnag02Bean;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.dao.Wkmnag02Df03Dao;
import com.humansystem.waku2.exception.Waku2RuntimeException;
import com.humansystem.waku2.util.CryptoUtils;
import com.humansystem.waku2.util.MailUtils;
import com.humansystem.waku2.util.StringUtils;

/**
 * WKMNAG02/参加者募る画面のActionクラス
 * <p>
 * WKMNAG02_EV_02
 * <p/>
 *
 * 説明：DBへの登録、及びメールの送信処理を行う
 *
 * create-date 21/05/2017.
 *
 * @author HumanSystemCo.,Ltd.
 */

public class Wkmnag02Ev02Action extends BaseAction {

	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkmnag02Ev02Action.class);

	/**
	 * リクエスト情報から、画面項目の情報を取得し、Beanに設定する
	 */
	@Override
	public BaseBean populate(HttpServletRequest req, HttpServletResponse resp, Connection con) {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "populate()");

		Wkmnag02Bean bean = (Wkmnag02Bean) req.getAttribute("__bean__");

		if (bean == null) {

			bean = new Wkmnag02Bean();

		}

		String mem = req.getParameter("memberContact");

		if (mem == null) {
			mem = "0";
		}

		bean.setMemberContact(mem);

		bean.setChouseiUrl(req.getParameter("chouseiUrl"));
		bean.setMemberContact(mem);
		bean.setMailText(req.getParameter("mailText"));
		bean.setEventMngrMailAddr(req.getParameter("eventMngrMailAddr"));
		bean.setParticipantMailaddress(req.getParameter("participantMailaddress"));
		bean.setAllMembersSendMail(req.getParameter("allMembersSendMail"));
		bean.setCryptedEventId(req.getParameter("cryptedEventId"));
		bean.setContactChangeFlag(req.getParameter("contactChangeFlag"));
		bean.setEventKey(req.getParameter("eventKey"));
		bean.setApplDeadline(req.getParameter("applDeadline"));
		bean.setAnsweredMailaddress(req.getParameter("answeredMailaddress"));

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "populate()");

		return bean;
	}

	/**
	 * 入力チェックを行う
	 */
	@Override
	public void validate(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "validate()");

		String target = ((Wkmnag02Bean) obj).getMailText();
		String name = "メールテキスト";

		// 最大
		if (target.length() > 1000) {
			obj.addError(ImmutableValues.format("MSGERR0002", new String[] { name, "1000" }));
		}
		if (target != null && checkXSS(target)) {
			((Wkmnag02Bean) obj).setMailText("");
			obj.addError(ImmutableValues.format("MSGERR0017", new String[] { name }));
		}

		target = ((Wkmnag02Bean) obj).getParticipantMailaddress();
		name = "参加者メールアドレス";

		// 最大
		if (target != null && target.length() > 4000) {

			obj.addError(ImmutableValues.format("MSGERR0002", new String[] { name, "4000" }));

		} else {

			if (StringUtils.isEmpty(target) == false) {
				String[] str = target.split(",");

				for (int i = 0; i < str.length; i++) {
					if (!checkMlAddr(str[i])) {

						obj.addError(ImmutableValues.format("MSGERR0017", new String[] { name, "1000" }));
						break;
					}

					if (str[i].length() > 256) {

						obj.addError(ImmutableValues.format("MSGERR0002", new String[] { name, "256" }));
						break;

					}
					if (checkXSS(str[i])) {
						((Wkmnag02Bean) obj).setParticipantMailaddress("");
						obj.addError(ImmutableValues.format("MSGERR0017", new String[] { name }));
					}
				}
			}
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "validate()");

	}

	private boolean checkMlAddr(String address) {

		boolean result;
		String aText = "[\\w!#%&'/=~`\\*\\+\\?\\{\\}\\^\\$\\-\\|]";
		String dotAtom = aText + "+" + "(\\." + aText + "+)*";
		String regularExpression = "^" + dotAtom + "@" + dotAtom + "$";
		result = checkMailAddr(address, regularExpression);

		return result;

	}

	private boolean checkMailAddr(String address, String regularExpression) {

		Pattern pattern = Pattern.compile(regularExpression);
		Matcher matcher = pattern.matcher(address);

		if (matcher.find()) {

			return true;

		}

		return false;

	}

	private boolean checkXSS(String value) {
		boolean result = false;
		String[] splitValue = value.split("");
		String aText = ".*[<>&\"'].*";
		for (String chr : splitValue) {
			if (chr.matches(aText)) {
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * 画面項目の登録処理とメールを送るフラグがたっていれば、メールの送信処理を行う
	 */
	@Override
	public BaseBean process(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "process()");

		// エラーの場合
		if (obj.getError() != null && obj.getError().size() > 0) {

			return obj;

		}

		Wkmnag02Bean bean = (Wkmnag02Bean) obj;

		String eventKey = bean.getEventKey();

		String eventID = CryptoUtils.decrypto(bean.getCryptedEventId());

		int updCnt = 0;
		ArrayList<String> bindDaoUpdate = new ArrayList<String>();

		bindDaoUpdate.add(bean.getMemberContact());
		bindDaoUpdate.add(bean.getMailText());
		bindDaoUpdate.add(bean.getParticipantMailaddress());

		bindDaoUpdate.add(eventID);

		// SQL実行
		Wkmnag02Df03Dao update = new Wkmnag02Df03Dao();
		updCnt = update.update(con, bindDaoUpdate);

		// エラー判定
		if (updCnt == 0) {

			throw new Waku2RuntimeException(ImmutableValues.format("MSGFTL0005", new String[] { "更新" }));

		}

		// メールの送信
		if (bean.getMemberContact().equals("1")) {

			String[] paAddr = StringUtils.isEmpty(bean.getParticipantMailaddress()) ? new String[0]
					: bean.getParticipantMailaddress().split(",");

			String[] dlAddr = StringUtils.isEmpty(bean.getAnsweredMailaddress()) ? new String[0]
					: bean.getAnsweredMailaddress().split("<br>");

			String[] mlAddr = StringUtils.isEmpty(CryptoUtils.decrypto(bean.getEventMngrMailAddr())) ? new String[0]
					: CryptoUtils.decrypto(bean.getEventMngrMailAddr()).split(",");

			// 送信メールアドレスを除外
			if (bean.getAllMembersSendMail() == null) {

				for (String removeAddr : dlAddr) {

					paAddr = removeAddress(paAddr, removeAddr);
					mlAddr = removeAddress(mlAddr, removeAddr);

				}
			}

			// 参加者にメール送信
			String text = bean.getMailText();
			String deadLine = bean.getApplDeadline();
			for (String paMail : paAddr) {
				if (StringUtils.isEmpty(paMail) == false) {
					MailUtils.sendMailToCand(paMail, eventKey, text, deadLine);
				}
			}

			// 管理者にメール送信
			for (String mlMail : mlAddr) {
				if (StringUtils.isEmpty(mlMail) == false) {
					MailUtils.sendMailToCand(mlMail, eventKey, text, deadLine);
				}
			}
		}

		bean.setRedirectURL("/waku2/manage/WKMNAG03.html?ek=" + bean.getEventKey());

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "process()");

		return bean;

	}

	private String[] removeAddress(String[] addresses, String deleteAddr) {

		List<String> addressList = new ArrayList<>(Arrays.asList(addresses));
		addressList.remove(deleteAddr);

		return addressList.toArray(new String[0]);

	}

}
