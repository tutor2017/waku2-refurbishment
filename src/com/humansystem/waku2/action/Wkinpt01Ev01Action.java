package com.humansystem.waku2.action;

import java.sql.Connection;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.humansystem.waku2.bean.BaseBean;
import com.humansystem.waku2.bean.Wkinpt01Bean;
import com.humansystem.waku2.common.ImmutableValues;
import com.humansystem.waku2.dao.Wkinpt01Df01Dao;
import com.humansystem.waku2.dao.Wkinpt01Df02Dao;
import com.humansystem.waku2.dao.Wkinpt01Df03Dao;
import com.humansystem.waku2.dao.Wkinpt01Df04Dao;
import com.humansystem.waku2.exception.Waku2RuntimeException;
import com.humansystem.waku2.util.CryptoUtils;
import com.humansystem.waku2.util.StringUtils;

/**
 * WKINPT01/わくわく調整表のBeanクラス
 *
 * 説明：わくわく調整表の○△×入力結果の一覧表示
 *
 * create-date 01/05/2017.
 *
 * @author 作成者:ishii_y 修正者:morita_k
 */

public class Wkinpt01Ev01Action extends BaseAction {
	/** Logger */
	private static Logger log = LoggerFactory.getLogger(Wkinpt01Ev01Action.class);

	/**
	 * パラメータからekとmを取得し、設定する
	 */
	@Override
	public BaseBean populate(HttpServletRequest req, HttpServletResponse resp, Connection con) {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "populate()");

		Wkinpt01Bean bean = new Wkinpt01Bean();

		// イベントキーを取得
		if (StringUtils.isEmpty(req.getParameter("ek")) == false) {
			bean.setEventKey(req.getParameter("ek"));
		} else {
			throw new Waku2RuntimeException(ImmutableValues.MSGFTL0008);
		}
		// メールアドレス取得
		if (StringUtils.isEmpty(req.getParameter("m")) == false) {
			bean.setEventApplAddr(req.getParameter("m"));
		}

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "populate()");

		return bean;

	}

	/**
	 * 入力チェックなし
	 */
	@Override
	public void validate(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		// 入力チェック無し
	}

	/**
	 * 回答者の回答情報を抽出し、画面に設定する
	 */
	@Override
	public BaseBean process(HttpServletRequest req, HttpServletResponse resp, BaseBean obj, Connection con) {
		// 開始ログ
		log.info(ImmutableValues.MSGLOG0003, "process()");
		Wkinpt01Bean bean = (Wkinpt01Bean) obj;

		List<String> bindValuesDf01 = new ArrayList<String>();
		String eventId = null;
		String mlAddr = null;
		String cntctFlg = null;
		String paAddr = null;

		// イベントキー
		bindValuesDf01.add(bean.getEventKey());

		// イベントキーからイベント情報を取得
		List<java.util.Map<String, String>> resultDf01 = new Wkinpt01Df01Dao().select(con, bindValuesDf01);
		if (resultDf01.size() == 0 || resultDf01 == null) {
			throw new Waku2RuntimeException(ImmutableValues.format("MSGERR0021", new String[] { "申込締切" }));
		} else {
			bean.setEventName(resultDf01.get(0).get(Wkinpt01Df01Dao.EVENT_NAME));
			bean.setEventMemo(resultDf01.get(0).get(Wkinpt01Df01Dao.EVENT_MEMO));
			eventId = resultDf01.get(0).get(Wkinpt01Df01Dao.EVENT_ID);
			bean.setCryptedEventId(CryptoUtils.encrypto(eventId));
			bean.setApplDeadline(resultDf01.get(0).get(Wkinpt01Df01Dao.APPL_DEADLINE));
			mlAddr = resultDf01.get(0).get(Wkinpt01Df01Dao.EVENT_MNGR_MAIL_ADDR);
			cntctFlg = resultDf01.get(0).get(Wkinpt01Df01Dao.CNTCT_FLG);
			paAddr = resultDf01.get(0).get(Wkinpt01Df01Dao.CNTCT_MAIL_ADDR);
		}

		String[] mlAddrs = StringUtils.isEmpty(mlAddr) ? new String[0] : mlAddr.split(",");
		String[] paAddrs = StringUtils.isEmpty(paAddr) ? new String[0] : paAddr.split(",");

		String urlPraMail = CryptoUtils.decrypto(bean.getEventApplAddr());


		if (cntctFlg.equals("1") && (!containsAddress(mlAddrs, urlPraMail) && !containsAddress(paAddrs, urlPraMail))) {
			throw new Waku2RuntimeException(ImmutableValues.MSGFTL0008);
		}

		// 全体数を求める
		int allMemberCnt = paAddrs.length + mlAddrs.length;
		bean.setAllMemberCount(String.valueOf(allMemberCnt));

		// 〆切を一日以上過ぎているか判定
		String deadline = bean.getApplDeadline();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dl = Date.valueOf(deadline);
		Calendar cal = Calendar.getInstance();
	//	cal.add(Calendar.DAY_OF_MONTH);
		String nowdate = sdf.format(cal.getTime());
		Date nd = Date.valueOf(nowdate);
		int diff = dl.compareTo(nd);
		if (diff < 0) {
			bean.setCntctFlg("1");
			bean.addError(ImmutableValues.format("MSGERR0021", new String[] { "期日" }));
		} else {
			bean.setCntctFlg("0");
		}

		// Dao2
		// 日程--------------------------------------------------------------------------------------------------------------
		List<String> bindValuesDf02 = new ArrayList<String>();
		// イベントID
		bindValuesDf02.add(eventId);

		// イベント日程を取得
		List<java.util.Map<String, String>> resultDf02 = new Wkinpt01Df02Dao().select(con, bindValuesDf02);

		List<Wkinpt01Bean> eventDateTextList = new ArrayList<Wkinpt01Bean>();

		// イベント日程を画面項目にセット
		for (java.util.Map<String, String> m : resultDf02) {
			Wkinpt01Bean wkinp = new Wkinpt01Bean();
			wkinp.setEventDateID(m.get(Wkinpt01Df02Dao.EVENT_DATE_ID));
			wkinp.setEventDateText(m.get(Wkinpt01Df02Dao.EVENT_DATE_TEXT));
			eventDateTextList.add(wkinp);
		}
		bean.setEventDateList(eventDateTextList);
		System.out.println("登録データ数:" + eventDateTextList.size());

		// Dao03
		// 出欠情報を取得---------------------------------------------------------------------------------------------------

		List<String> bindValuesDf03 = new ArrayList<String>();
		// イベントID
		bindValuesDf03.add(eventId);

		// イベントIDからイベント情報を取得
		List<java.util.Map<String, String>> resultDf03 = new Wkinpt01Df03Dao().select(con, bindValuesDf03);

		List<Wkinpt01Bean> eventDateTextList03 = new ArrayList<Wkinpt01Bean>();

		// イベント情報を画面項目にセット
		for (java.util.Map<String, String> m : resultDf03) {
			Wkinpt01Bean wkinp3 = new Wkinpt01Bean();
			wkinp3.setEventDateID(m.get(Wkinpt01Df03Dao.EVENT_DATE_ID));
			wkinp3.setCryptedEventAttendid(CryptoUtils.encrypto(m.get(Wkinpt01Df03Dao.EVENT_ATTEND_ID)));
			wkinp3.setApplType(m.get(Wkinpt01Df03Dao.APPL_TYPE));
			eventDateTextList03.add(wkinp3);
		}
		bean.setEventDateList1(eventDateTextList03);

		// 行ごとのカウント数を格納
		for (Wkinpt01Bean date : bean.getEventDateList()) {
			int maruCnt = 0;
			int sankakuCnt = 0;
			int batsuCnt = 0;
			for (Wkinpt01Bean attend : bean.getEventDateList1()) {
				if (date.getEventDateID().equals(attend.getEventDateID())) {
					switch (attend.getApplType()) {
					case "1":
						maruCnt++;
						break;
					case "2":
						sankakuCnt++;
						break;
					case "3":
						batsuCnt++;
						break;
					default:
						break;
					}
				}
			}
			date.setMaruCount(String.valueOf(maruCnt));
			date.setSankakuCount(String.valueOf(sankakuCnt));
			date.setBatuCount(String.valueOf(batsuCnt));
		}

		// Dao04
		// 名前コメント---------------------------------------------------------------------------------------------------------

		List<String> bindValuesDf04 = new ArrayList<String>();
		// イベントID
		bindValuesDf04.add(eventId);

		// イベントIDからイベント情報を取得
		List<java.util.Map<String, String>> resultDf04 = new Wkinpt01Df04Dao().select(con, bindValuesDf04);
		List<Wkinpt01Bean> eventDateTextList04 = new ArrayList<Wkinpt01Bean>();
		List<Wkinpt01Bean> eventDateTextList042 = new ArrayList<Wkinpt01Bean>();
		// イベント情報を画面項目にセット
		for (java.util.Map<String, String> m : resultDf04) {
			Wkinpt01Bean wkinp4 = new Wkinpt01Bean();
			Wkinpt01Bean wkinp42 = new Wkinpt01Bean();
			wkinp4.setCryptedEventAttendid(CryptoUtils.encrypto(m.get(Wkinpt01Df04Dao.EVENT_ATTEND_ID)));
			wkinp4.setApplName(m.get(Wkinpt01Df04Dao.APPL_NAME));
			wkinp42.setApplComment(m.get(Wkinpt01Df04Dao.APPL_COMMENT));
			wkinp4.setEventApplAddr(CryptoUtils.encrypto(m.get(Wkinpt01Df04Dao.APPL_MAIL_ADDR)));

			if (bean.getEventApplAddr().equals("")) {
				wkinp4.setRedirectURL("/waku2/input/WKINPT02.html?ek=" + bean.getEventKey() + "&ceai="
						+ wkinp4.getCryptedEventAttendid());
			} else {
				wkinp4.setRedirectURL("/waku2/input/WKINPT02.html?ek=" + bean.getEventKey() + "&ceai="
						+ wkinp4.getCryptedEventAttendid() + "&m=" + bean.getEventApplAddr());

			}
			eventDateTextList04.add(wkinp4);
			eventDateTextList042.add(wkinp42);
		}
		bean.setEventDateList2(eventDateTextList04);
		bean.setEventDateList3(eventDateTextList042);

		// 終了ログ
		log.info(ImmutableValues.MSGLOG0004, "process()");
		return bean;
	}


	private boolean containsAddress(String[] addresses, String checkAddr) {

		List<String> addressList = new ArrayList<>(Arrays.asList(addresses));
		if (addressList.contains(checkAddr)) {
			return true;
		} else {
			return false;
		}
	}

}